﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lexicon.CircularProgressBar
{
    /// <summary>
    /// Логика взаимодействия для CircularProgressBar4.xaml
    /// </summary>
    public partial class CircularProgressBar4 : UserControl
    {
        static async void CircularProgressBar3()
        {
            //Use a default Animation Framerate of 30, which uses less CPU time
            //than the standard 50 which you get out of the box
            Timeline.DesiredFrameRateProperty.OverrideMetadata(
                    typeof(Timeline), new FrameworkPropertyMetadata
                    {
                        DefaultValue = 30
                    });
            await Task.Delay(30);
        }

        public CircularProgressBar4()
        {
            InitializeComponent();
        }
    }
}
