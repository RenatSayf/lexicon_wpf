﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lexicon.Classes;
using Lexicon.Model;
using System.Windows.Media.Animation;
using System.Diagnostics;

namespace Lexicon
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static string DataBaseName;
        public static AddWord winAddWord;

        private string _nameDB = "\\WordsDB.db";
        private SpeakingClass _speak;
        private BackgroundWorker _worker = new BackgroundWorker();
        private static PlayListClass _playList = new PlayListClass();
        public static Func<List<string>> _funcPlayList = _playList.KeptPlayList2();

        public static SQLiteDataBaseClass _sqLiteDataBaseClass;

        private Rect bounds;

        //-----------------------------------------------------------------------------------------
        public MainWindow()
        {
            InitializeComponent();
            _speak = new SpeakingClass(txtblock1, txtblock2);
            _sqLiteDataBaseClass = new SQLiteDataBaseClass();
            
            try
            {
                bounds = Properties.Settings.Default.windowPosition;
                MainWindow1.Top = bounds.Top;
                MainWindow1.Left = bounds.Left;
                if (bounds.Width != 0 && bounds.Height != 0)
                {
                    MainWindow1.Width = bounds.Width;
                    MainWindow1.Height = bounds.Height;
                }
            }
            catch (Exception)
            {
                
            }

            SliderSaund.Width = 0;
            SliderSaund.Value = Properties.Settings.Default.sliderValue;
        }

        private async void Play_Click(object sender, RoutedEventArgs e)
        {
            Data.IsPlay = true;
            _speak.PlayStop = true;
            await _speak.Play(Properties.Settings.Default.playBackOrder, Data.Ndict, Data.Nword, Data.AfterPause);
            var a = _funcPlayList();
        }

        private void btnPause_Click(object sender, RoutedEventArgs e)
        {
            _speak.Pause();
        }
        
        private void BtnForvard_Click(object sender, RoutedEventArgs e)
        {
            if (Properties.Settings.Default.playBackOrder == 0)
            {
                _speak.GetNextWord();
            }
            else
            {
                _speak.GetPeviousWord();
            }
        }
        
        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            if (Properties.Settings.Default.playBackOrder == 0)
            {
                _speak.GetPeviousWord();
            }
            else
            {
                _speak.GetNextWord();
            }
        }
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        private void MainWindow1_Closed(object sender, EventArgs e)
        {
            //System.Windows.Application.Current.Shutdown();
            bounds.Size = new Size(Width, Height);
            bounds.Location = new Point(this.Left, this.Top);
            Properties.Settings.Default.windowPosition = bounds;
            Properties.Settings.Default.Save();
        }
        //-----------------------------------------------------------------------------------------
        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            //System.Windows.Application.Current.Shutdown();
        }
        //-----------------------------------------------------------------------------------------
        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            //Task.Run(() => _speak.PlayStop = false);
            Task.Run(() => _speak.Stop());
        }
        //-----------------------------------------------------------------------------------------
        private void MainWindow1_Loaded(object sender, RoutedEventArgs e)
        {
            string dataBaseName = Directory.GetCurrentDirectory() + _nameDB;
        Agan:
            if (!File.Exists(dataBaseName))
            {
                SQLiteConnection.CreateFile(dataBaseName);
                Properties.Settings.Default.settingsPlayList.Clear();
                goto Agan;

            }
            else
            {
                DataBaseName = dataBaseName;
                _sqLiteDataBaseClass.CreateTable("This Application");
                _sqLiteDataBaseClass.CreateTable("Everyday");
                _sqLiteDataBaseClass.CreateTable("Informatics");
                _sqLiteDataBaseClass.CreateTable("Travel");
            }

            MainWindow1.Topmost = Properties.Settings.Default.isTopMost;
            if (MainWindow1.Topmost)
            {
                BtnSpike.Style = FindResource("VerticalPinButton") as Style;
            }
            else
            {
                BtnSpike.Style = FindResource("HorizontalPinButton") as Style;
            }
        }
        
        private void MainWindow1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                this.DragMove();
            }
            catch { }
        }

        private Storyboard sbSliderWidth;

        private void btnSaund_MouseEnter(object sender, MouseEventArgs e)
        {
            SliderSaund.Visibility = Visibility.Visible;
            if (SliderSaund.Width == 0)
            {
                sbSliderWidth = Resources["sbSliderMax"] as Storyboard;
                sbSliderWidth.Begin(SliderSaund);
            }
            
        }
        
        private void btnPlayList_Click(object sender, RoutedEventArgs e)
        {
            PlayListView playListView = new PlayListView();
            playListView.Owner = this;
            playListView.Show();
        }

        private Storyboard sboard;
        private Storyboard sboard2;
        private Storyboard animTextPanelMin;
        private Storyboard animShowBottomMenu;
        private bool flag = false;

        private void MainWindow1_MouseEnter(object sender, MouseEventArgs e)
        {
            flag = false;
            if (sboard != null && sboard2 != null)
            {
                sboard.Pause();
                sboard2.Pause();
            }
            animShowBottomMenu = Resources["sbShowBottomMenu"] as Storyboard;
            animTextPanelMin = Resources["sbTextPanelMin"] as Storyboard;
            
            if (pnlBottomMenu.Margin.Bottom == -40)
            {
                animShowBottomMenu.Begin(pnlBottomMenu);
                animTextPanelMin.Begin(viewBox1); 
            }
        }

        private void Sboard_Completed(object sender, EventArgs e)
        {
            if (SliderSaund.Width != 0)
            {
                sbSliderWidth = Resources["sbSliderMin"] as Storyboard;
                sbSliderWidth.Begin(SliderSaund);
            }
            //Debug.WriteLine("Сработал Sboard_Completed");
        }

        private async void MainWindow1_MouseLeave(object sender, MouseEventArgs e)
        {
            flag = true;
            sboard = Resources["sbHideBottomMenu"] as Storyboard;
            sboard2 = Resources["sbTextPanelMax"] as Storyboard;
            if (sboard2 != null && sboard != null)
            {
                sboard.Completed += Sboard_Completed; 
            }
            
            await Task.Delay(2000);
            if (pnlBottomMenu.Margin.Bottom == 0 && flag)
            {
                sboard.Begin(pnlBottomMenu); 
                sboard2.Begin(viewBox1);
                flag = false;
            }
        }
        
        private void ButtonMenu_Click(object sender, RoutedEventArgs e)
        {
            contextMenu.IsOpen = true;
        }

        private void MenuItem1_Click(object sender, RoutedEventArgs e)
        {
            if (winAddWord == null)
            {
                winAddWord = new AddWord();
                winAddWord.tabAddRemoveDict.Focus();
                winAddWord.Show();
            }
            else
            {
                winAddWord.tabAddRemoveDict.Focus();
            }
        }

        private void MenuItem2_Click(object sender, RoutedEventArgs e)
        {
            if (winAddWord == null)
            {
                winAddWord = new AddWord();
                winAddWord.tabAddWord.Focus();
                winAddWord.Show();
            }
            else
            {
                winAddWord.tabAddWord.Focus();
            }
        }

        private async void MenuItem3_Click(object sender, RoutedEventArgs e)
        {
            if (winAddWord == null)
            {
                winAddWord = new AddWord();
                winAddWord.TabEditWord.Focus();
                winAddWord.Show();
            }
            else
            {
                winAddWord.TabEditWord.Focus();
            }

            try
            {
                Data.CurrentWord = await _speak.GetCurrentWord();
                Data.CurrentTableWord = await _speak.GetCurrentWord2();
                var a = Data.CurrentNameDict;
            }
            catch (Exception)
            {
                return;
            }
        }

        private void MenuItem4_Click(object sender, RoutedEventArgs e)
        {
            if (winAddWord == null)
            {
                winAddWord = new AddWord(); 
                winAddWord.TabCheckYourSelf.Focus();
                winAddWord.Show();
            }
            else
            {
                winAddWord.TabCheckYourSelf.Focus();
            }
        }

        private void MenuItem5_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.Save();
            System.Windows.Application.Current.Shutdown();
        }

        private void btn_Saund_Click(object sender, RoutedEventArgs e)
        {
            if (SliderSaund.Value != 0)
            {
                Properties.Settings.Default.sliderValue = SliderSaund.Value;
                Properties.Settings.Default.Save();
                SliderSaund.Value = 0;
                btn_Saund.Style = FindResource("SoundButtonOff") as Style; 
            }
            else
            {
                SliderSaund.Value = Properties.Settings.Default.sliderValue;
                btn_Saund.Style = FindResource("SoundButtonOn") as Style;
            }
        }

        private void SliderSaund_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            _speak.SpeechVolume = (int)e.NewValue;   
        }

        private void SliderSaund_MouseLeave(object sender, MouseEventArgs e)
        {
            if (SliderSaund.Value == 0)
            {
                Properties.Settings.Default.sliderValue = SliderSaund.Value;
                btn_Saund.Style = FindResource("SoundButtonOff") as Style;
            }
            else
            {
                Properties.Settings.Default.sliderValue = SliderSaund.Value;
                btn_Saund.Style = FindResource("SoundButtonOn") as Style;
            }
        }

        private void BtnSpike_Click(object sender, RoutedEventArgs e)
        {
            if (MainWindow1.Topmost)
            {
                MainWindow1.Topmost = false;
                BtnSpike.Style = FindResource("HorizontalPinButton") as Style;
            }
            else
            {
                MainWindow1.Topmost = true;
                BtnSpike.Style = FindResource("VerticalPinButton") as Style;
            }
            Properties.Settings.Default.isTopMost = MainWindow1.Topmost;
        }
    }















    #region Data

    public static class Data
    {
        public static ObservableCollection<CollectionWords_Model> CurrentTableWord = new ObservableCollection<CollectionWords_Model>();
        private static List<string> _currentWord = new List<string> { "", "", "", "", "", "", "", "", "" };

        public static List<string> CurrentWord
        {
            get { return _currentWord; }
            set { _currentWord = value; }
        }

        public static bool IsPlay
        {
            get;
            set;
        }

        public static int Ndict { get; set; }

        private static int _nword = 1;
        public static int Nword
        {
            get { return _nword; }
            set { _nword = value; }
        }

        public static string CurrentNameDict { get; set; }

        public static bool AfterPause { get; set; }

        private static bool _isTopMost = Properties.Settings.Default.isTopMost;
        public static bool IsTopMost
        {
            get { return _isTopMost; }
            set
            {
                _isTopMost = value;
                Properties.Settings.Default.isTopMost = _isTopMost;
            }
        }

        public static bool IsPlayListChanged { get; set; }

    }
    #endregion
}
