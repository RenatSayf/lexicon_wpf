﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using Lexicon.Classes;

namespace Lexicon.ViewModel
{
    public class TabItem2ViewModel : INotifyPropertyChanged
    {
        private SQLiteDataBaseClass _sqlDataBaseClass;
        private string _oldName;
        private int _oldIndex = -1;
        //-----------------------------------------------------------------------------------------
        public TabItem2ViewModel()
        {
            _sqlDataBaseClass = new SQLiteDataBaseClass();
        }
        //-----------------------------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        //-----------------------------------------------------------------------------------------
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        #region TabItem2
        private bool _tabIsSelected = true;
        public bool TabIsSelected
        {
            get { return _tabIsSelected; }
            set
            {
                if (value != _tabIsSelected)
                {
                    _tabIsSelected = value;
                    NotifyPropertyChanged("TabIsSelected");
                    if (_tabIsSelected)
                    {
                        GetComBoxItems.Execute(null);
                    }
                }
            }
        }

        private ICommand _getComBoxItems;
        public ICommand GetComBoxItems
        {
            get
            {
                return _getComBoxItems ?? (_getComBoxItems = new RelayCommand(async (arg) =>
                {
                    ListDict = await _sqlDataBaseClass.GetListTable();
                }));
            }
        }

        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region Button Button AddDict
        private ICommand btnAddDictClick;
        public ICommand BtnAddDictClick
        {
            get
            {
                return btnAddDictClick ?? (btnAddDictClick = new RelayCommand((arg) =>
                {
                    ListDict = Task.Run(async () => await _sqlDataBaseClass.GetListTable()).Result;
                }));
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region ComboBox Remove Dictionary
        private string _comBoxRemoveText = "Select";
        public string ComBoxRemoveText
        {
            get { return _comBoxRemoveText; }
            set
            {
                if (value != _comBoxRemoveText)
                {
                    _comBoxRemoveText = value;
                    NotifyPropertyChanged("ComBoxRemoveText");
                    
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private int _comBoxRemoveSelectedIndex = -1;
        public int ComBoxRemoveSelectedIndex
        {
            get { return _comBoxRemoveSelectedIndex; }
            set
            {
                if (value != _comBoxRemoveSelectedIndex)
                {
                    _comBoxRemoveSelectedIndex = value;
                    NotifyPropertyChanged("ComBoxRemoveSelectedIndex");
                    BtnRemoveIsEnabled = (_comBoxRemoveSelectedIndex != -1) ? true : false;
                }
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region Button Remove
        private ICommand _btnRemoveClick;
        public ICommand BtnRemoveClick
        {
            get
            {
                return _btnRemoveClick ?? (_btnRemoveClick = new RelayCommand((arg) =>
                {
                    ListDict = Task.Run(async () => await _sqlDataBaseClass.GetListTable()).Result;
                    BtnRenameIsEnabled = false;
                    ComBoxRenameText = null;
                    ComBoxRemoveSelectedIndex = -1;
                    ComBoxRenameSelectedIndex = -1;
                }));
            }
        }
        //-----------------------------------------------------------------------------------------
        private bool _btnRemoveIsEnabled;
        public bool BtnRemoveIsEnabled
        {
            get { return _btnRemoveIsEnabled; }
            set
            {
                if (value != _btnRemoveIsEnabled)
                {
                    _btnRemoveIsEnabled = value;
                    NotifyPropertyChanged("BtnRemoveIsEnabled");
                }
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region ComboBox Rename
        private List<string> _listDict;
        public List<string> ListDict
        {
            get { return _listDict; }
            set
            {
                if (value != _listDict)
                {
                    _listDict = value;
                    NotifyPropertyChanged("ListDict");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _comBoxRenameText = "New name";
        public string ComBoxRenameText
        {
            get { return _comBoxRenameText; }
            set
          {
                if (value != _comBoxRenameText)
                {
                    _comBoxRenameText = value;
                    NotifyPropertyChanged("ComBoxRenameText");
                    BtnRenameIsEnabled = (_comBoxRenameText != null && _comBoxRenameText != "" && _comBoxRenameText != ComBoxSelectItem) ? true : false;
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _comBoxSelectItem;
        public string ComBoxSelectItem
        {
            get { return _comBoxSelectItem; }
            set
            {
                if (value != _comBoxSelectItem)
                {
                    _comBoxSelectItem = value;
                    NotifyPropertyChanged("ComBoxSelectItem");
                    _oldName = _comBoxSelectItem;
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private int _comBoxRenameSelectedIndex = -1;
        public int ComBoxRenameSelectedIndex
        {
            get { return _comBoxRenameSelectedIndex; }
            set
            {
                if (value != _comBoxRenameSelectedIndex)
                {
                    _comBoxRenameSelectedIndex = value;
                    NotifyPropertyChanged("ComBoxRenameSelectedIndex");
                    if (_comBoxRenameSelectedIndex >= 0)
                    {
                        _oldIndex = _comBoxRenameSelectedIndex;
                    }
                }
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region Button Rename
        private ICommand _btnRenameClick;
        public ICommand BtnRenameClick
        {
            get
            {
                return _btnRenameClick ?? (_btnRenameClick = new RelayCommand((arg) =>
                  {
                      string oldDict = ListDict[_oldIndex];
                      var res = _sqlDataBaseClass.SqlQueryRenameTable(ListDict[_oldIndex], ComBoxRenameText);
                      ListDict = Task.Run(async () => await _sqlDataBaseClass.GetListTable()).Result;
                      ComBoxRemoveSelectedIndex = -1;
                      BtnRenameIsEnabled = false;
                      if (Properties.Settings.Default.settingsPlayList.Contains(oldDict))
                      {
                          Properties.Settings.Default.settingsPlayList.Remove(oldDict);
                          Properties.Settings.Default.Save();
                      }
                      ComBoxRenameText = null;
                      Task.Run(async () => await LabelDamping());
                  }));
            }
        }
        //-----------------------------------------------------------------------------------------
        private bool _btnRenameIsEnabled;
        public bool BtnRenameIsEnabled
        {
            get { return _btnRenameIsEnabled; }
            set
            {
                if (value != _btnRenameIsEnabled)
                {
                    _btnRenameIsEnabled = value;
                    NotifyPropertyChanged("BtnRenameIsEnabled");
                }
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region Label Well Done
        private double _labelOpacity = 0.0;
        public double LabelOpacity
        {
            get { return _labelOpacity; }
            set
            {
                if (value != _labelOpacity)
                {
                    _labelOpacity = value;
                    NotifyPropertyChanged("LabelOpacity");
                }
            }
        }
        //----------------------------------------------------------------------------------------
        private async Task LabelDamping()
        {
            LabelOpacity = 100.0;
            while (LabelOpacity > 0)
            {
                LabelOpacity -= 20.0;
                await Task.Delay(500);
            }
        }
        #endregion


    }
}
