# Lexicon #
### Операционная система: Windows 8.1, 10 ###
### Технология: Windows Presentation Foundation (C#) ###

Обучающее приложение Lexicon предназначено для быстрого изучения и запоминания слов английского языка.
![main-window.png](https://bitbucket.org/repo/kgeALb/images/1183861744-main-window.png)

### Приложение имеет следующие функции: ###
* Динамичное отображение на экране английских слов с русскоязычным переводом и синхронное озвучивание их с использованием синтеза речи .NET Framework System.Speech
### ![main-window2.png](https://bitbucket.org/repo/kgeALb/images/301420647-main-window2.png) ###
### ![main-window3.png](https://bitbucket.org/repo/kgeALb/images/910958894-main-window3.png) ###
### ![main-window4.png](https://bitbucket.org/repo/kgeALb/images/3005694734-main-window4.png) ###
* Для хранения слов используется база данных SQLite. Слова группируются в словарях. Имеется возможность создания, удаления, переименования словарей:
### ![add-dictionary.png](https://bitbucket.org/repo/kgeALb/images/34059067-add-dictionary.png) ###

###   ###
###   ###
* Функция добавления слов используя онлайн перевод Google Translate:
### ![add-word.png](https://bitbucket.org/repo/kgeALb/images/2963377-add-word.png) ###
* Редактор слов:
### ![words-editor.png](https://bitbucket.org/repo/kgeALb/images/1785029363-words-editor.png) ###

* Тест для проверки знаний:
### ![test.png](https://bitbucket.org/repo/kgeALb/images/3038870173-test.png) ###
* Можно настроить "play-list", выбрав необходимые словари из списка доступных, Задать порядок воспроизведения - прямой или обратный, упорядочить словари по своему желанию:
### ![play-list.png](https://bitbucket.org/repo/kgeALb/images/980594779-play-list.png) ###