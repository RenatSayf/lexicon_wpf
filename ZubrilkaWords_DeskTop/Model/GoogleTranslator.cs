﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Net.Http;
using System.Diagnostics;
using System.Net;

namespace Lexicon.Model
{
    public class GoogleTranslator
    {
        private static GoogleTranslator _googleTranslator;

        private GoogleTranslator()
        {

        }

        static GoogleTranslator()
        {
            _googleTranslator = new GoogleTranslator();
        }

        public static GoogleTranslator Instance
        {
            get { return _googleTranslator; }
        }

        public async Task<HtmlDocument> SendHttpPostRequest(string url)
        {
            Uri uri = new Uri(url);
            HttpClient _httpClient = new HttpClient();
            HttpResponseMessage response = new HttpResponseMessage();
            HtmlDocument document = new HtmlDocument();
            HtmlWeb hw = new HtmlWeb();
            var a = hw.Load(url);
            try
            {
                //response = await _httpClient.PostAsync(url, null);
                response = await _httpClient.GetAsync(url);
                var bytes = await response.Content.ReadAsByteArrayAsync();
                String source = Encoding.GetEncoding("utf-8").GetString(bytes, 0, bytes.Length - 1);
                source = WebUtility.HtmlDecode(source);

                document.LoadHtml(source);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Исключение в SendHttpGetRequest() - " + ex.Message);
                document = null;
            }

            return document;
        }


    }
}
