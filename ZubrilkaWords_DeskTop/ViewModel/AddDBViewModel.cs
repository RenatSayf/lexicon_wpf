﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Lexicon.Classes;
using Lexicon.Model;

namespace Lexicon.ViewModel
{
    public class AddDBViewModel : INotifyPropertyChanged
    {
        private SQLiteDataBaseClass _sqliteDataBaseClass;
        private AddDBModel _addDBModel;
        private EditWordModel _editWordModel;
        private ObservableCollection<CollectionWords_Model> _tableSourceSelectedItems;
        //-----------------------------------------------------------------------------------------
        public AddDBViewModel()
        {
            TableSourceItemSelect = new ObservableCollection<CollectionWords_Model>();
            TableReceiver = new ObservableCollection<CollectionWords_Model>();
            _tableSourceSelectedItems = new ObservableCollection<CollectionWords_Model>();
            _sqliteDataBaseClass = new SQLiteDataBaseClass();
            _addDBModel = new AddDBModel();
            _editWordModel = new EditWordModel();
            ComboBox2 = Task.Run(async () => await _sqliteDataBaseClass.GetListTable()).Result;
        }
        //-----------------------------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        //-----------------------------------------------------------------------------------------
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        //-----------------------------------------------------------------------------------------
        #region TabItem Add Data Base
        private bool _tabIsSelected = true;
        public bool TabIsSelected
        {
            get { return _tabIsSelected; }
            set
            {
                if (value != _tabIsSelected)
                {
                    _tabIsSelected = value;
                    NotifyPropertyChanged("TabIsSelected");
                    if (_tabIsSelected)
                    {
                        ComboBox2 = Task.Run(async () => await _sqliteDataBaseClass.GetListTable()).Result;
                    }
                }
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region Button Open
        private ICommand _btnOpenClick;
        public ICommand BtnOpenClick
        {
            get
            {
                return _btnOpenClick ?? (_btnOpenClick = new RelayCommand(async (arg) =>
                {
                    OpenFileDialog dlg = new OpenFileDialog();
                    dlg.InitialDirectory = "C:\\";
                    dlg.DefaultExt = ".db"; // Default file extension
                    dlg.Filter = "Data Base files (.db)|*.db"; // Filter files by extension
                    // Show open file dialog box
                    var result = dlg.ShowDialog();
                    // Process open file dialog box results
                    if (result == true)
                    {
                        // Open document
                        string filename = dlg.FileName;
                        ComboBox1 = await _sqliteDataBaseClass.GetListTable(filename);
                        ComBox1SelectedItem = ComboBox1[0];
                        if (ComboBox1.Count > 0)
                        {
                            DataView table = await _sqliteDataBaseClass.GetTableFromDb(filename, ComBox1SelectedItem);
                            if (_addDBModel.CheckingTableStructure(table))
                            {
                                TextBlockText = filename;
                                TableFromDb = table;
                                BtnSelectAllEnabled = true;
                            }
                            else { MessageBox.Show("You open a file that has an irregular structure.\nSelect another file"); }
                        }
                    }
                }));
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region Button Close
        private ICommand _btnCloseClick;
        public ICommand BtnCloseClick
        {
            get
            {
                return _btnCloseClick ?? (_btnCloseClick = new RelayCommand( (arg) =>
                {
                    
                }));
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region DataGrid 1
        public ObservableCollection<CollectionWords_Model> TableSourceItemSelect { get; set; }
        //-----------------------------------------------------------------------------------------
        private int _dataGrid1SelectedIndex = -1;
        public int DataGrid1SelectedIndex
        {
            get { return _dataGrid1SelectedIndex; }
            set
            {
                if (value != _dataGrid1SelectedIndex)
                {
                    _dataGrid1SelectedIndex = value;
                    NotifyPropertyChanged("DataGrid1SelectedIndex");
                    if (_dataGrid1SelectedIndex >= 0)
                    {
                        BtnAddInIsEnabled = true;
                        TableSourceItemSelect =_addDBModel.SelectWordFromOtherTable(TableFromDb, _dataGrid1SelectedIndex);
                        ObservableCollection<CollectionWords_Model> t = new ObservableCollection<CollectionWords_Model>();
                        t.Add(TableSourceItemSelect[0]);
                        _tableSourceSelectedItems.Add(TableSourceItemSelect[0]);
                        TableReceiver.Add(TableSourceItemSelect[0]);
                    }
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private DataView _tableFromDb;
        //-----------------------------------------------------------------------------------------
        public DataView TableFromDb
        {
            get { return _tableFromDb; }
            set
            {
                if (value != _tableFromDb)
                {
                    _tableFromDb = value;
                    NotifyPropertyChanged("TableFromDB");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private Selector _dataGrid1SelectedItem;
        public Selector DataGrid1SelectedItem
        {
            get { return _dataGrid1SelectedItem; }
            set
            {
                if (value != _dataGrid1SelectedItem)
                {
                    _dataGrid1SelectedItem = value;
                    NotifyPropertyChanged("DataGrid1SelectedItem");
                    
                }
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region DataGrid 2
        public ObservableCollection<CollectionWords_Model> TableReceiver { get; set; }
        //-----------------------------------------------------------------------------------------
        private int _dataGrid2SelectedIndex;
        public int DataGrid2SelectedIndex
        {
            get { return _dataGrid2SelectedIndex; }
            set
            {
                if (value != _dataGrid2SelectedIndex)
                {
                    _dataGrid2SelectedIndex = value;
                    NotifyPropertyChanged("DataGrid2SelectedIndex");
                    if (_dataGrid2SelectedIndex >= 0)
                    {
                        BtnDeleteIsEnabled = true;
                    }
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private Selector _dataGrid2SelectedItem;
        public Selector DataGrid2SelectedItem
        {
            get { return _dataGrid2SelectedItem; }
            set
            {
                if (value != _dataGrid2SelectedItem)
                {
                    _dataGrid2SelectedItem = value;
                    NotifyPropertyChanged("DataGrid2SelectedItem");
                    
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private DataView _tableFromNotifyDb;
        //-----------------------------------------------------------------------------------------
        public DataView TableNotifyFromDb
        {
            get { return _tableFromNotifyDb; }
            set
            {
                if (value != _tableFromNotifyDb)
                {
                    _tableFromNotifyDb = value;
                    NotifyPropertyChanged("TableNotifyFromDb");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private double _dataGrid2Opacity = 100;
        public double DataGridOpacity
        {
            get { return _dataGrid2Opacity; }
            set
            {
                if (value != _dataGrid2Opacity)
                {
                    _dataGrid2Opacity = value;
                    NotifyPropertyChanged("DataGridOpacity");
                }
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region TextBlock
        private string _textBlockText;
        public string TextBlockText
        {
            get { return _textBlockText; }
            set
            {
                if (value != _textBlockText)
                {
                    _textBlockText = value;
                    NotifyPropertyChanged("TextBlockText");
                }
            }
        }

        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region ComboBox 1
        private List<string> _comboBox1;
        //-----------------------------------------------------------------------------------------
        public List<string> ComboBox1
        {
            get { return _comboBox1; }
            set
            {
                if (value != _comboBox1)
                {
                    _comboBox1 = value;
                    NotifyPropertyChanged("ComboBox1");
                }   
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _comBox1SelectedItem;
        //-----------------------------------------------------------------------------------------
        public string ComBox1SelectedItem
        {
            get { return _comBox1SelectedItem; }
            set
            {
                if (value != _comBox1SelectedItem)
                {
                    _comBox1SelectedItem = value;
                    NotifyPropertyChanged("ComBox1SelectedItem");
                    DataView table = Task.Run(async () => await _sqliteDataBaseClass.GetTableFromDb(TextBlockText, ComBox1SelectedItem)).Result;
                    if (_addDBModel.CheckingTableStructure(table))
                    {
                        TableFromDb = table;
                        BtnSelectAllEnabled = true;
                    }
                    else { MessageBox.Show("You open a file that has an irregular structure.\nSelect another file"); }
                }
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region ComboBox 2
        private List<string> _comboBox2;
        //-----------------------------------------------------------------------------------------
        public List<string> ComboBox2
        {
            get { return _comboBox2; }
            set
            {
                if (value != _comboBox2)
                {
                    _comboBox2 = value;
                    NotifyPropertyChanged("ComboBox2");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _comBox2SelectedItem;
        //-----------------------------------------------------------------------------------------
        public string ComBox2SelectedItem
        {
            get { return _comBox2SelectedItem; }
            set
            {
                if (value != _comBox2SelectedItem)
                {
                    _comBox2SelectedItem = value;
                    NotifyPropertyChanged("ComBox2SelectedItem");
                    if (ComboBox2.Count > 0 && _comBox2SelectedItem != null && ComBox2Text != "New dictionary")
                    {
                        TableNotifyFromDb = Task.Run(async()=>  await _sqliteDataBaseClass.GetTableFromDb(_comBox2SelectedItem)).Result;
                    }
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _comBox2Text = "New dictionary";
        //-----------------------------------------------------------------------------------------
        public string ComBox2Text
        {
            get { return _comBox2Text; }
            set
            {
                if (value != _comBox2Text)
                {
                    _comBox2Text = value;
                    NotifyPropertyChanged("ComBox2Text");
                }
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region Button Select All
        private bool _btnSelectAllEnabled;
        public bool BtnSelectAllEnabled
        {
            get { return _btnSelectAllEnabled; }
            set
            {
                if (value != _btnSelectAllEnabled)
                {
                    _btnSelectAllEnabled = value;
                    NotifyPropertyChanged("BtnSelectAllEnabled");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private ICommand _btnSelectAllClick;
        public ICommand BtnSelectAllClick
        {
            get
            {
                return _btnSelectAllClick ?? (_btnSelectAllClick = new RelayCommand((arg) =>
                {
                    if (TableFromDb.Count > 0)
                    {
                        TableReceiver.Clear();
                        var table = _addDBModel.SelectAllWordFromOtherTable(TableFromDb);
                        foreach (var item in table)
                        {
                            TableReceiver.Add(item);
                        }
                    }
                    if(TableReceiver.Count > 0) BtnAddInIsEnabled = true;
                }));
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region Button Add In
        private bool _btnAddInIsEnabled;
        public bool BtnAddInIsEnabled
        {
            get { return _btnAddInIsEnabled; }
            set
            {
                if (value != _btnAddInIsEnabled)
                {
                    _btnAddInIsEnabled = value;
                    NotifyPropertyChanged("BtnAddInIsEnabled");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private ICommand _btnAddInClick;
        public ICommand BtnAddInAllClick
        {
            get
            {
                return _btnAddInClick ?? (_btnAddInClick = new RelayCommand(async(arg) =>
                {
                    if (TableReceiver.Count > 0)
                    {
                        await _sqliteDataBaseClass.CreateTableAsync(ComBox2Text);
                        LabelIsVisible = Visibility.Visible;
                        DataGridOpacity = 30;
                        int i = 0;
                        LabelContent += "  " + i;
                        foreach (var item in TableReceiver)
                        {
                            if (ComBox2Text == "New dictionary") { MessageBox.Show("Задайте имя словаря\n или выберите из списка"); break; }
                            i += await _sqliteDataBaseClass.AddWordInTableAsync(ComBox2Text/* ComBox2SelectedItem*/, item);
                        }
                        if (i == TableReceiver.Count)
                        {
                            BtnAddInIsEnabled = false;
                            ComBox2Text = "New dictionary";
                            LabelContent = "Well Done!!!";
                            TableReceiver.Clear();
                            DataGridOpacity = 100;
                            ComboBox2 = Task.Run(async () => await _sqliteDataBaseClass.GetListTable()).Result;
                            await Task.Delay(2000);
                            LabelIsVisible = Visibility.Collapsed;
                            LabelContent = "Added";
                            
                        }
                        else
                        {
                            LabelContent = "Words have not been added";
                            await Task.Delay(2000);
                            LabelIsVisible = Visibility.Collapsed;
                        }
                    }
                }));
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region Buttton Delete
        private bool _btnDeleteIsEnabled;
        public bool BtnDeleteIsEnabled
        {
            get { return _btnDeleteIsEnabled; }
            set
            {
                if (value != _btnDeleteIsEnabled)
                {
                    _btnDeleteIsEnabled = value;
                    NotifyPropertyChanged("BtnDeleteIsEnabled");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private ICommand _btnDeleteClick;
        public ICommand BtnDeleteClick
        {
            get
            {
                return _btnDeleteClick ?? (_btnDeleteClick = new RelayCommand((arg) =>
                {
                    var item = DataGrid2SelectedIndex;
                    TableReceiver.RemoveAt(item);
                    BtnDeleteIsEnabled = false;
                    if (TableReceiver.Count == 0) BtnAddInIsEnabled = false;
                }));
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region Label Added
        private Visibility _labelIsVisible = Visibility.Collapsed;
        public Visibility LabelIsVisible
        {
            get { return _labelIsVisible; }
            set
            {
                if (value != _labelIsVisible)
                {
                    _labelIsVisible = value;
                    NotifyPropertyChanged("LabelIsVisible");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _labelContent = "Added";
        public string LabelContent
        {
            get { return _labelContent; }
            set
            {
                if (value != _labelContent)
                {
                    _labelContent = value;
                    NotifyPropertyChanged("LabelContent");
                }
            }
        }
        #endregion



    }
}
