﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace Lexicon.Classes
{
    public class PlayListClass : INotifyPropertyChanged
    {
        private SQLiteDataBaseClass _sqliteDBClass;
        private List<string> _listDictionary;
        private StringCollection _keptPlayList;
        private List<string> _keptPlayList2 = new List<string>();
        private StringCollection _playListOld = new StringCollection();// = Properties.Settings.Default.settingsPlayList;

        //-----------------------------------------------------------------------------------------
        public PlayListClass()
        {
            _sqliteDBClass = new SQLiteDataBaseClass();
            Task.Run(async() => ListDictionary = await _sqliteDBClass.GetListTable()); 
            _ordeplay = Properties.Settings.Default.playBackOrder;
            _keptPlayList = Properties.Settings.Default.settingsPlayList;
            _playListOld = Properties.Settings.Default.settingsPlayList;
        }
        //-----------------------------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        //-----------------------------------------------------------------------------------------
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        //-----------------------------------------------------------------------------------------
        public List<string> ListDictionary
        {
            get
            {
                //Task.Run(async () => _listDictionary = await _sqliteDBClass.GetListTable());
                return _listDictionary;
            }
            set
            {
                if (value != _listDictionary)
                {
                    _listDictionary = value;
                    NotifyPropertyChanged("ListDictionary");
                }
                
            }
        }
        //-----------------------------------------------------------------------------------------
        public List<string> KeptPlayList
        {
            get { return GetPlayList(); }
            set
            {
                //OnPropertyChanged(new PropertyChangedEventArgs("ListTable"));
            }
        }
        //-----------------------------------------------------------------------------------------
        public List<string> GetPlayList()
        {
            for (int i = 0; i < _keptPlayList.Count; i++)
            {
                _keptPlayList2.Add(_keptPlayList[i].ToString());
            }
            return _keptPlayList2;
        }
        //-----------------------------------------------------------------------------------------
        public Func<List<string>> KeptPlayList2()
        {
            Func<List<string>> func = () => GetPlayList();
            return func;
        }
        //-----------------------------------------------------------------------------------------
        public List<string> SortPlayList()
        {
            List<string> list = new List<string>();

            return list;
        }
        //-----------------------------------------------------------------------------------------
        public void SelectItemFromDict(ListBox lbxListDict, ListBox lbxPlayList)
        {
            string _selectDict = lbxListDict.SelectedItems[0].ToString();
            if (!Properties.Settings.Default.settingsPlayList.Contains(_selectDict))
            {
                Properties.Settings.Default.settingsPlayList.Add(_selectDict);
                Properties.Settings.Default.Save();
            }
            lbxPlayList.ItemsSource = null;
            lbxPlayList.ItemsSource = Properties.Settings.Default.settingsPlayList;
        }
        //-----------------------------------------------------------------------------------------
        public void SavePlayList(ListBox lbxPlayList)
        {
            string firstItem;
            try
            {
                firstItem = Properties.Settings.Default.settingsPlayList[0].ToString();
            }
            catch (Exception) { firstItem = null; }
            
            Properties.Settings.Default.settingsPlayList.Clear();
            if (lbxPlayList.Items.Count > 0)
            {
                foreach (var item in lbxPlayList.Items)
                {
                    Properties.Settings.Default.settingsPlayList.Add(item.ToString());
                    Properties.Settings.Default.Save();
                }
            }
            try
            {
                if (Properties.Settings.Default.settingsPlayList[0].ToString() != firstItem && Data.IsPlay)
                {
                    Data.IsPlayListChanged = true;
                }
            }
            catch (ArgumentOutOfRangeException) { Data.IsPlayListChanged = false; };
        }
        //-----------------------------------------------------------------------------------------
        public void СleansePlaylist(ListBox lbxPlayList)
        {
            lbxPlayList.ItemsSource = null;
            SavePlayList(lbxPlayList);
        }
        //-----------------------------------------------------------------------------------------
        public List<string> DelOneItemFromPL(ListBox lbxPlayList, int index)
        {
            List<string> list = new List<string>();
            try
            {
                for (int i = 0; i < lbxPlayList.Items.Count; i++)
                {
                    list.Add(lbxPlayList.Items[i].ToString());
                }
                list.RemoveAt(lbxPlayList.SelectedIndex);
                lbxPlayList.ItemsSource = null;
            }
            catch { }
            SavePlayList(lbxPlayList);
            return list;
        }
        //-----------------------------------------------------------------------------------------
        public void ItemUp(ListBox lbxPlayList)
        {
            List<string> list = new List<string>();
            int index = lbxPlayList.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            string buffer = "";

            if (lbxPlayList.Items.Count > 1 && lbxPlayList.SelectedIndex != 0)
            {
                for (int i = 0; i < lbxPlayList.Items.Count; i++)
                {
                    list.Add(lbxPlayList.Items[i].ToString());
                }
                int newindex = index - 1;
                buffer = list[index];
                list[index] = list[newindex];
                list[newindex] = buffer;

                lbxPlayList.ItemsSource = null;
                lbxPlayList.ItemsSource = list;
                lbxPlayList.SelectedIndex = newindex;
            }
            SavePlayList(lbxPlayList);
        }
        //-----------------------------------------------------------------------------------------
        public void ItemDown(ListBox lbxPlayList)
        {
            List<string> list = new List<string>();
            int index = lbxPlayList.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            string buffer = "";

            if (lbxPlayList.Items.Count > 1 && lbxPlayList.SelectedIndex != lbxPlayList.Items.Count - 1)
            {
                for (int i = 0; i < lbxPlayList.Items.Count; i++)
                {
                    list.Add(lbxPlayList.Items[i].ToString());
                }
                int newindex = index + 1;
                buffer = list[index];
                list[index] = list[newindex];
                list[newindex] = buffer;

                lbxPlayList.ItemsSource = null;
                lbxPlayList.ItemsSource = list;
                lbxPlayList.SelectedIndex = newindex;
            }
            SavePlayList(lbxPlayList);
        }
        //-----------------------------------------------------------------------------------------
        private int _ordeplay;
        //-----------------------------------------------------------------------------------------
        public int OrderPlay
        {
            get
            {
                return _ordeplay;
            }
            set
            {
                if (value != _ordeplay)
                {
                    _ordeplay = value;
                    Properties.Settings.Default.playBackOrder = _ordeplay;
                    Data.IsPlayListChanged = true;
                    NotifyPropertyChanged("OrderPlay");
                }
            }
        }

        private bool _popupIsOpen;
        public bool PopupIsOpen
        {
            get { return _popupIsOpen; }
            set
            {
                if (value != _popupIsOpen)
                {
                    _popupIsOpen = value;
                    NotifyPropertyChanged("PopupIsOpen");
                }
            }
        }

        private ICommand _btnAddCommand;
        public ICommand BtnAddCommand
        {
            get
            {
                return _btnAddCommand ?? (_btnAddCommand = new RelayCommand((arg) =>
                {
                    PopupIsOpen = true;
                }));
            }
        }

        private string _accessDictSelectedItem;
        public string AccessDictSelectedItem
        {
            get { return _accessDictSelectedItem; }
            set
            {
                if (value != _accessDictSelectedItem)
                {
                    _accessDictSelectedItem = value;
                    NotifyPropertyChanged("AccessDictSelectedItem");
                    
                }
            }
        }





    }
}
