﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Runtime.CompilerServices;
using System.Xml;

namespace Lexicon.Model
{
    public class TranslatorModel : INotifyPropertyChanged
    {
        public ObservableCollection<CollectionWords_Model> NewWord { get; set; }

        private string _httpResult;
        
        //-----------------------------------------------------------------------------------------
        public TranslatorModel()
        {
            NewWord = new ObservableCollection<CollectionWords_Model>();
            //BtnAddWordIsEnable = true;
        }
        //-----------------------------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        //-----------------------------------------------------------------------------------------
        private void NotifyPropertyChanged([CallerMemberName]string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        //-----------------------------------------------------------------------------------------
        private Uri CreateRequestToGoogle(string param)
        {
            Uri request = null;
            if (param != null)
            {
                param = param.ToLower();
                string sl = "en", tl = "ru";
                if (param.Any(wordByte => wordByte > 127))
                {
                    sl = "ru"; tl = "en";
                }
                string url = @"https://translate.google.com/translate_a/single?client=t&sl=" + sl + "&tl=" + tl + "&hl=ru&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&dt=at&ie=UTF-8&oe=UTF-8&source=btn&srcrom=1&ssel=0&tsel=0&kc=1&tk=523163|176597&q=" + param;
                request = new Uri(url);
            }
            return request;
        }
        //-----------------------------------------------------------------------------------------
        public void SendRequestToGoogle(string param)
        {
            Uri uri = CreateRequestToGoogle(param);
            if (uri == null) return;
            WebClient client = new WebClient();
            client.Headers.Add("authority:translate.google.com");
            client.Headers.Add("method:GET");
            client.Headers.Add("scheme:https");
            client.Headers.Add("accept:*/*");
            //client.Headers.Add("accept-encoding:gzip, deflate, sdch");
            client.Headers.Add("accept-language:ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
            client.Headers.Add("dnt:1");
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            client.Headers.Add("x-chrome-uma-enabled:1");

            client.OpenReadCompleted += new OpenReadCompletedEventHandler(OpenReadResponseGoogle);
            client.OpenReadAsync(uri);
        }
        //-----------------------------------------------------------------------------------------
        public async Task SendRequestToGoogleAsync(string param)
        {
            var httpClient = new HttpClient();
            Uri uri = CreateRequestToGoogle(param);
            if (uri == null) return;
            WebClient client = new WebClient();
            client.Headers.Add("authority:translate.google.com");
            client.Headers.Add("method:GET");
            client.Headers.Add("scheme:https");
            client.Headers.Add("accept:*/*");
            //client.Headers.Add("accept-encoding:gzip, deflate, sdch");
            client.Headers.Add("accept-language:ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
            client.Headers.Add("dnt:1");
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            client.Headers.Add("x-chrome-uma-enabled:1");
            Stream data = await httpClient.GetStreamAsync(uri);

            _httpResult = "";
            StreamReader reader = new StreamReader(data);
            _httpResult = reader.ReadToEnd();
            data.Close();
            reader.Close();
            DataGridSetSource(_httpResult);
        }
        //-----------------------------------------------------------------------------------------
        private void OpenReadResponseGoogle(object sender, OpenReadCompletedEventArgs e)
        {
            _httpResult = "";
            Stream data = e.Result;
            StreamReader reader = new StreamReader(data);
            _httpResult = reader.ReadToEnd();
            //data.Close();
            reader.Close();

            DataGridSetSource(_httpResult);
        }
        //-----------------------------------------------------------------------------------------
        public string ExtractFirstWord(string httpResult)
        {
            string firstWord = "";
            Match math = Regex.Match(httpResult, "\".+?\"", RegexOptions.Singleline);
            firstWord = math.Value;
            firstWord = firstWord.Trim('"');
            FirstWordProperty = firstWord;
            return firstWord;
        }
        //-----------------------------------------------------------------------------------------
        public string FirstWordProperty{ get; set; }
        //-----------------------------------------------------------------------------------------
        private string HttpResultParse(string httpResult, string param)
        {
            string result = "";
            Match math = Regex.Match(httpResult, param + ".+?]", RegexOptions.Singleline);
            string res1 = math.Value;
            Match math2 = Regex.Match(res1, ",.+?]", RegexOptions.Singleline);
            string res2 = math2.Value;
            res2 = res2.Trim(',');
            res2 = res2.TrimEnd(']');
            res2 = res2.TrimStart('[');
            res2 = res2.Trim();
            res2 = res2.Replace('"', ' ');
            char[] div = { ',' };
            string[] parts = res2.Split(div);
            string res3 = "";
            if (parts.Length > 1)
            {
                for (int i = 0; i < 2; i++)
                {
                    res3 += parts[i].Trim() + ", ";
                }
                res3 = res3.Trim();
                res3 = res3.Trim(',');
                res3 = res3.Trim();
                result = res3;
            }
            else
            {
                res3 += parts[0].Trim();
                res3 = res3.Trim();
                result = res3.Trim(',');
                res3 = res3.Trim();
            }

            return result;
        }
        //-----------------------------------------------------------------------------------------
        public async void GetWordsFromGoogle(string param)
        {
            _inputText = param;
            //SendRequestToGoogle(param);
            await SendRequestToGoogleAsync(param);
        }
        //-----------------------------------------------------------------------------------------
        private string _inputText;
        public void DataGridSetSource(string param)
        {
            if (param == "") return;
            NewWord.Clear();
            CollectionWords_Model model = new CollectionWords_Model();
            model.English = _inputText;
            model.Translation = ExtractFirstWord(param);
            model.Noun = HttpResultParse(param, "имя существительное");
            model.Verb = HttpResultParse(param, "глагол");
            model.Adjective = HttpResultParse(param, "имя прилагательное");
            model.Adverb = HttpResultParse(param, "наречие");
            model.Pronoun = HttpResultParse(param, "местоимение");
            model.Preposition = HttpResultParse(param, "предлог");
            NewWord.Add(model);

            if (NewWord[0].Translation.Any(wordByte => wordByte > 127))
            {
                if (NewWord[0].Noun.Any(wordByte => wordByte <= 122 && wordByte >= 65)) { NewWord[0].Noun = ""; }
                if (NewWord[0].Verb.Any(wordByte => wordByte <= 122 && wordByte >= 65)) { NewWord[0].Verb = ""; }
                if (NewWord[0].Adjective.Any(wordByte => wordByte <= 122 && wordByte >= 65)) { NewWord[0].Adjective = ""; }
                if (NewWord[0].Adverb.Any(wordByte => wordByte <= 122 && wordByte >= 65)) { NewWord[0].Adverb = ""; }
                if (NewWord[0].Pronoun.Any(wordByte => wordByte <= 122 && wordByte >= 65)) { NewWord[0].Pronoun = ""; }
                if (NewWord[0].Preposition.Any(wordByte => wordByte <= 122 && wordByte >= 65)) { NewWord[0].Preposition = ""; }
            }
        }
        //-----------------------------------------------------------------------------------------
        public ObservableCollection<CollectionWords_Model> DataGridSetSource2(string text, string trans)
        {
            //if (text == "") return;
            NewWord.Clear();
            CollectionWords_Model model = new CollectionWords_Model();
            model.English = text;
            //model.Translation = trans;
            List<string> list = new List<string>();

            XmlDocument docXml = new XmlDocument();
            if (trans != null && trans != "")
            {
                docXml.LoadXml(trans);
                foreach (XmlNode node in docXml.SelectNodes("span"))
                {
                    foreach (XmlNode item in node.ChildNodes)
                    {
                        list.Add(item.InnerText);
                    }
                }
            }
            
            if (list.Count > 0)
            {
                model.Translation = list[0];
            }
            model.Noun = ""; model.Verb = ""; model.Adjective = ""; model.Adverb = ""; model.Pronoun = ""; model.Preposition = "";
            NewWord.Add(model);

            return NewWord;

            //if (NewWord[0].Translation.Any(wordByte => wordByte > 127))
            //{
            //    if (NewWord[0].Noun.Any(wordByte => wordByte <= 122 && wordByte >= 65)) { NewWord[0].Noun = ""; }
            //    if (NewWord[0].Verb.Any(wordByte => wordByte <= 122 && wordByte >= 65)) { NewWord[0].Verb = ""; }
            //    if (NewWord[0].Adjective.Any(wordByte => wordByte <= 122 && wordByte >= 65)) { NewWord[0].Adjective = ""; }
            //    if (NewWord[0].Adverb.Any(wordByte => wordByte <= 122 && wordByte >= 65)) { NewWord[0].Adverb = ""; }
            //    if (NewWord[0].Pronoun.Any(wordByte => wordByte <= 122 && wordByte >= 65)) { NewWord[0].Pronoun = ""; }
            //    if (NewWord[0].Preposition.Any(wordByte => wordByte <= 122 && wordByte >= 65)) { NewWord[0].Preposition = ""; }
            //}
        }






    }
}
