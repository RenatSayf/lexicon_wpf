﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Media;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Windows.Media;
using System.Windows;
using Lexicon.Classes;
using Lexicon.ViewModel;

namespace Lexicon.Model
{
    public class SpeechModel
    {
        private SQLiteDataBaseClass _sqliteDataBaseClass;
        private SpeechSynthesizer _synthesizer;
        private bool _play = true;
        private string _enText;
        private string _transText;
        private List<string> _voiceEn = new List<string>();
        private List<string> _voiceRu = new List<string>();
        private Brush _enTextForeground = Brushes.Black;
        

        //-----------------------------------------------------------------------------------------
        public SpeechModel(/*string enText, string transText/*, Brush enTextForeground*/)
        {
            //EnText = enText;
            //TransText = transText;
            //_enTextForeground = enTextForeground;
            _sqliteDataBaseClass = new SQLiteDataBaseClass();
            _synthesizer = new SpeechSynthesizer();// создание объекта типа SpeechSynthesizer
            CreateListOfVoices();
        }
        //-----------------------------------------------------------------------------------------
        public string EnText
        {
            get { return _enText; }
            set { _enText = value; }
        }
        //-----------------------------------------------------------------------------------------
        public string TransText
        {
            get { return _transText; }
            set { _transText = value; }
        }
        //-----------------------------------------------------------------------------------------
        private void CreateListOfVoices()
        {
            foreach (InstalledVoice voice in _synthesizer.GetInstalledVoices())// обход коллекции установленых голосов
            {
                VoiceInfo infoVoice = voice.VoiceInfo; // получаем информацию о каждом голосе
                if (infoVoice.Culture.Name.Equals("en-US"))
                {
                    _voiceEn.Add(infoVoice.Name);
                }
                else if (infoVoice.Culture.Name.Equals("ru-RU"))
                {
                    _voiceRu.Add(infoVoice.Name);
                }
            }
            if (_voiceEn.Count <= 0)
            {
                MessageBox.Show(@"В Вашей системе отсутствует англоязычный(US) ситезатор речи./n
                                    Голосовые функции для этого синтезатора недоступны./n
                                    Установите голосовой пакет путем обновления Windows.");
            }
            if (_voiceRu.Count <= 0)
            {
                MessageBox.Show(@"В Вашей системе отсутствует русскоязычный(RU) ситезатор речи./n
                                    Голосовые функции для этого синтезатора недоступны./n
                                    Установите голосовой пакет путем обновления Windows.");
            }
        }
        //-----------------------------------------------------------------------------------------
        public bool PlayStop
        {
            get { return _play; }
            set { _play = value; }
        }
        //-----------------------------------------------------------------------------------------
        public Task task = Task.Run(() => { });
        //-----------------------------------------------------------------------------------------
        public async Task<List<string>> GetWordsFromDict(string nameDict, int index)
        {
            List<string> _listWord = new List<string>();
            _listWord = await _sqliteDataBaseClass.GetWordFromTable(nameDict, index);
            return _listWord;
        }
        //-----------------------------------------------------------------------------------------
        private int _Ndict = 0, _Nword = 1;
        //-----------------------------------------------------------------------------------------
        public async Task Play(
                                int switch_on,          // Выбор слов в словаре 0 - последовательный выбор
                                                        //                      1 - обратный выбор
                                int Ndict,              // № номер словаря для начала воспроизведения (0 - с начала)
                                int Nword,              // № номер слова в словаре (1 - с начала)
                                bool afterPause         // true - воспроизведение после паузы, иначе false
                              )
        {
            List<string> rowWords = new List<string>();
            //_transText.Foreground = Brushes.Blue;
            //_enTextForeground = Brushes.Green;
            bool speechCompleted = false;
            
            if (Properties.Settings.Default.settingsPlayList.Count == 0)
            {
                _enText = "Set a playlist";
                //_enTextForeground = Brushes.Red;
                _transText = "";
                return;
            }
            switch (switch_on)
            {
                case 0:
                    while (_play)
                    {
                        if (!_play || Properties.Settings.Default.settingsPlayList.Count == 0) break;
                        for (int i = 0; i < Properties.Settings.Default.settingsPlayList.Count; i++)
                        {
                            if (!_play || Properties.Settings.Default.settingsPlayList.Count == 0) break;
                            try
                            {
                                int j = 1;
                                do
                                {
                                    if (!await CheckPlayListOnEmpty(i)) { _play = false; break; }

                                    _transText = "";
                                    if (afterPause)
                                    {
                                        rowWords = await GetWordsFromDict(Properties.Settings.Default.settingsPlayList[_Ndict], _Nword);
                                        afterPause = false;
                                        i = _Ndict;
                                        j = _Nword;
                                    }
                                    else
                                    {
                                        rowWords = await GetWordsFromDict(Properties.Settings.Default.settingsPlayList[i], j);
                                    }

                                    for (int z = 0; z < rowWords.Count; z++)
                                    {
                                        if (!_play || Properties.Settings.Default.settingsPlayList.Count == 0)
                                        {
                                            _Ndict = i;
                                            _Nword = j;
                                            break;
                                        }
                                        if (rowWords[z] == "") continue;
                                        _indexDict = i;
                                        _indexWord = j;
                                        speechCompleted = await SpeakWord(rowWords[z], _enText, _transText);
                                        while (!speechCompleted)
                                        {
                                            await Task.Delay(10);
                                        }
                                    }
                                    if (!_play || Properties.Settings.Default.settingsPlayList.Count == 0) break;
                                    speechCompleted = await SpeakWord(rowWords[0], _enText, _transText);
                                    while (!speechCompleted)
                                    {
                                        await Task.Delay(10);
                                    }
                                    j++;

                                } while (j <= await _sqliteDataBaseClass.NumberRecords(Properties.Settings.Default.settingsPlayList[i]));
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                _play = false;
                                break;
                            }
                        }
                    }
                    break;
                case 1:
                    while (_play)
                    {
                        if (!_play || Properties.Settings.Default.settingsPlayList.Count == 0) break;
                        for (int i = 0; i < Properties.Settings.Default.settingsPlayList.Count; i++)
                        {
                            if (!_play || Properties.Settings.Default.settingsPlayList.Count == 0) break;
                            try
                            {
                                int j = await _sqliteDataBaseClass.NumberRecords(Properties.Settings.Default.settingsPlayList[i]);
                                do
                                {
                                    if (!await CheckPlayListOnEmpty(i)) { _play = false; break; }

                                    _transText = "";
                                    if (afterPause)
                                    {
                                        rowWords = await GetWordsFromDict(Properties.Settings.Default.settingsPlayList[_Ndict], _Nword);
                                        afterPause = false;
                                        i = _Ndict;
                                        j = _Nword;
                                    }
                                    else
                                    {
                                        rowWords = await GetWordsFromDict(Properties.Settings.Default.settingsPlayList[i], j);
                                    }

                                    for (int z = 0; z < rowWords.Count; z++)
                                    {
                                        if (!_play || Properties.Settings.Default.settingsPlayList.Count == 0)
                                        {
                                            _Ndict = i;
                                            _Nword = j;
                                            break;
                                        }
                                        if (rowWords[z] == "") continue;
                                        _indexDict = i;
                                        _indexWord = j;
                                        speechCompleted = await SpeakWord(rowWords[z], _enText, _transText);
                                        while (!speechCompleted)
                                        {
                                            await Task.Delay(10);
                                        }
                                    }
                                    if (!_play || Properties.Settings.Default.settingsPlayList.Count == 0) break;
                                    speechCompleted = await SpeakWord(rowWords[0], _enText, _transText);
                                    while (!speechCompleted)
                                    {
                                        await Task.Delay(10);
                                    }
                                    j--;

                                } while (j >= 1);
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                _play = false;
                                break;
                            }
                        }
                    }
                    break;
            }
        }
        //-----------------------------------------------------------------------------------------
        private async Task<bool> SpeakWord(string str, string enText, string transText)
        {
            Prompt res = null;
            int time = Encoding.Unicode.GetByteCount(str) * 60;
            if (time < 2000)
            {
                time = 2000;
            }

            if (str.Any(wordByte => wordByte > 127))
            {
                _synthesizer.SelectVoice(_voiceRu[0]);
                //transText = str;
                _transText = str;
            }
            else
            {
                _synthesizer.SelectVoice(_voiceEn[0]);
                //enText = str + " - ";
                _enText = str + " - ";
            }
            res = _synthesizer.SpeakAsync(str);
           
            await Task.Delay(time);
            if (!res.IsCompleted)
            {
                await Task.Delay(10);
            }
            return true;
        }
        //-----------------------------------------------------------------------------------------
        public async Task<bool> SpeakWord(string str)
        {
            Prompt res = null;
            int time = Encoding.Unicode.GetByteCount(str);
            if (str.Any(wordByte => wordByte > 127))
            {
                _synthesizer.SelectVoice(_voiceRu[0]);
            }
            else
            {
                _synthesizer.SelectVoice(_voiceEn[0]);
            }
            res = _synthesizer.SpeakAsync(str);
            await Task.Delay(time + 50);
            //while (!res.IsCompleted)
            //{
            //    await Task.Delay(1);
            //}
            return true;
        }
        //-----------------------------------------------------------------------------------------
        private async Task<bool> CheckPlayListOnEmpty(int index)
        {
            string name = Properties.Settings.Default.settingsPlayList[index];
            int numberRecords = await _sqliteDataBaseClass.NumberRecords(name);
            if (numberRecords == 0)
            {
                _play = false;
                _transText = "";
                _enTextForeground = Brushes.Red;
                _enText = string.Format(@"Dictionary <<{0}>> is empty", name);
                return false;
            }
            return true;
        }
        //-----------------------------------------------------------------------------------------
        private int _indexWord = 1;
        private int _indexDict = 0;
        //-----------------------------------------------------------------------------------------
        public void Stop()
        {
            _play = false;
            _synthesizer.SpeakAsyncCancelAll();
            _synthesizer.Dispose();
            Data.IsPlay = false;
            Data.Ndict = 0;
            Data.Nword = 1;
            Data.AfterPause = false;
        }
        //-----------------------------------------------------------------------------------------
        public void Pause()
        {
            Stop();
            Data.Ndict = _indexDict;
            Data.Nword = _indexWord;
            Data.AfterPause = true;
        }

        
        //-----------------------------------------------------------------------------------------
    }
}
