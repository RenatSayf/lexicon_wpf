﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Lexicon.Classes;
using Lexicon.Model;
using System.Windows;

namespace Lexicon.ViewModel
{
    public class AddWordViewModel : INotifyPropertyChanged
    {
        private YandexTranslate _yandexTranslate;
        private YandexAPITranslate _yandexAPITranslate;
        private SQLiteDataBaseClass _sqlDataBaseClass;
        public ObservableCollection<CollectionWords_Model> NewWord { get; set; }
        public CollectionWords_Model wordModel = new CollectionWords_Model();
        private SpeakingClass _speakingClass;
        
        public AddWordViewModel()
        {
            _yandexTranslate = new YandexTranslate();
            _yandexAPITranslate = new YandexAPITranslate();
            _sqlDataBaseClass = new SQLiteDataBaseClass();
            _speakingClass = new SpeakingClass();

            YandexRefVisibility = Visibility.Collapsed;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        private bool _tabIsSelected = true;
        public bool TabIsSelected
        {
            get { return _tabIsSelected; }
            set
            {
                if (value != _tabIsSelected)
                {
                    _tabIsSelected = value;
                    NotifyPropertyChanged("TabIsSelected");
                    if (_tabIsSelected)
                    {
                        GetComBoxItems.Execute(null);
                    }
                }
            }
        }

        private string _textBox1;
        public string TextBox1
        {
            get { return _textBox1; }
            set
            {
                if (value != _textBox1)
                {
                    _textBox1 = value;
                    NotifyPropertyChanged("TextBox1");
                    VisibleTextTrans2 = Visibility.Collapsed;
                    VisibleTextTrans3 = Visibility.Collapsed;
                    TranslateCommand.Execute(null);
                    if (value.Length > 0)
                    {
                        VisibleSoundBtnInput = Visibility.Visible;
                        //BtnAddIsEnabled = true;
                    }
                    else
                    {
                        VisibleSoundBtnInput = Visibility.Collapsed;
                        VisibleSoundBtnOutput = Visibility.Collapsed;
                        //BtnAddIsEnabled = false;
                    }
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _textBox2;
        public string TextBox2
        {
            get { return _textBox2; }
            set
            {
                if (value != _textBox2)
                {
                    _textBox2 = value;
                    NotifyPropertyChanged("TextBox2");
                    if (value.Length > 0)
                    {
                        BtnAddIsEnabled = true;
                        YandexRefVisibility = Visibility.Visible;
                    }
                    else
                    {
                        VisibleSoundBtnOutput = Visibility.Collapsed;
                        BtnAddIsEnabled = false;
                        YandexRefVisibility = Visibility.Collapsed;
                    }
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _textBox3;
        public string TextBox3
        {
            get { return _textBox3; }
            set
            {
                if (value != _textBox3)
                {
                    _textBox3 = value;
                    NotifyPropertyChanged("TextBox3");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _textBox4;
        public string TextBox4
        {
            get { return _textBox4; }
            set
            {
                if (value != _textBox4)
                {
                    _textBox4 = value;
                    NotifyPropertyChanged("TextBox4");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _textBox5;
        public string TextBox5
        {
            get { return _textBox5; }
            set
            {
                if (value != _textBox5)
                {
                    _textBox5 = value;
                    NotifyPropertyChanged("TextBox5");
                }
            }
        }

        private List<string> _listDict;
        public List<string> ListDict
        {
            get { return _listDict; }
            set
            {
                if (value != _listDict)
                {
                    _listDict = value;
                    NotifyPropertyChanged("ListDict");
                }
            }
        }

        private string _comBoxSelectedItem;
        public string ComBoxSelectedItem
        {
            get { return _comBoxSelectedItem; }
            set
            {
                if (value != _comBoxSelectedItem)
                {
                    _comBoxSelectedItem = value;
                    NotifyPropertyChanged("ComBoxSelectedItem");
                }
            }
        }

        private bool _comBoxDropDownOpen = false;
        public bool ComBoxDropDownOpen
        {
            get { return _comBoxDropDownOpen; }
            set
            {
                if (value != _comBoxDropDownOpen)
                {
                    _comBoxDropDownOpen = value;
                    NotifyPropertyChanged("ComBoxDropDownOpen");
                }
            }
        }

        private ICommand _translateCommand;
        public ICommand TranslateCommand
        {
            get
            {
                return _translateCommand ?? (_translateCommand = new RelayCommand(async (arg) =>
                {
                    TextBox2 = ""; TextBox3 = ""; TextBox4 = ""; TextBox5 = "";
                    var res2 = await _yandexAPITranslate.GetTranslateFromYandex(_textBox1);
                    
                    if (res2.Count > 0)
                    {
                        TextBox2 = res2[0];
                        VisibleSoundBtnOutput = Visibility.Visible;
                    }

                    var res = await _yandexTranslate.GetTranslateFromYandex(_textBox1);
                    if (res.Count == 1)
                    {
                        //TextBox3 = res[0];
                    }
                    if (res.Count == 2)
                    {
                        //TextBox3 = res[0];
                        TextBox4 = res[1];
                        VisibleTextTrans2 = Visibility.Visible;
                    }
                    if (res.Count > 2)
                    {
                        TextBox3 = res[0];
                        TextBox4 = res[1];
                        TextBox5 = res[2];
                        VisibleTextTrans2 = Visibility.Visible;
                        VisibleTextTrans3 = Visibility.Visible;
                    }
                }));
            }
        }

        private ICommand _getComBoxItems;
        public ICommand GetComBoxItems
        {
            get
            {
                return _getComBoxItems ?? (_getComBoxItems = new RelayCommand(async (arg) =>
                {
                    ListDict = await _sqlDataBaseClass.GetListTable();
                }));
            }
        }

        private bool _btnAddIsEnabled = false;
        public bool BtnAddIsEnabled
        {
            get { return _btnAddIsEnabled; }
            set
            {
                if (value != _btnAddIsEnabled)
                {
                    _btnAddIsEnabled = value;
                    NotifyPropertyChanged("BtnAddIsEnabled");
                }
            }
        }

        private ICommand _addWordInDataBase;
        public ICommand AddWordInDataBase
        {
            get
            {
                return _addWordInDataBase ?? (_addWordInDataBase = new RelayCommand(async (arg) =>
                  {
                      if (ComBoxSelectedItem != null)
                      {
                          if (TextBox1.Any(wordByte => wordByte <= 122 && wordByte >= 65))
                          {
                              wordModel.Clean();
                              wordModel.English = TextBox1;
                              wordModel.Translation = TextBox2;
                              wordModel.Noun = TextBox4;
                              wordModel.Verb = TextBox5;
                              var res = await _sqlDataBaseClass.AddWordInTableAsync(ComBoxSelectedItem, wordModel);
                              if (res >= 0)
                              {
                                  LabelVisibility = Visibility.Visible;
                                  await Task.Delay(2000);
                                  LabelVisibility = Visibility.Collapsed;
                                  
                              }
                          }
                          else if (TextBox1.Any(wordByte => wordByte > 122))
                          {
                              wordModel.Clean();
                              wordModel.Translation = TextBox1;
                              wordModel.English = TextBox2;
                              var res = await _sqlDataBaseClass.AddWordInTableAsync(ComBoxSelectedItem, wordModel);
                              if (res >= 0)
                              {
                                  LabelVisibility = Visibility.Visible;
                                  await Task.Delay(2000);
                                  LabelVisibility = Visibility.Collapsed;
                              }
                          }
                      }
                      else
                      {
                          ComBoxDropDownOpen = true;
                      }
                  }));
            }
        }

        private Visibility _labelVisibility = Visibility.Collapsed;
        public Visibility LabelVisibility
        {
            get { return _labelVisibility; }
            set
            {
                if (value != _labelVisibility)
                {
                    _labelVisibility = value;
                    NotifyPropertyChanged("LabelVisibility");
                }
            }
        }

        private Visibility _visibleTextTrans2 = Visibility.Collapsed;
        public Visibility VisibleTextTrans2
        {
            get { return _visibleTextTrans2; }
            set
            {
                if (value != _visibleTextTrans2)
                {
                    _visibleTextTrans2 = value;
                    NotifyPropertyChanged("VisibleTextTrans2");
                }
            }
        }

        private Visibility _visibleTextTrans3 = Visibility.Collapsed;
        public Visibility VisibleTextTrans3
        {
            get { return _visibleTextTrans3; }
            set
            {
                if (value != _visibleTextTrans3)
                {
                    _visibleTextTrans3 = value;
                    NotifyPropertyChanged("VisibleTextTrans3");
                }
            }
        }

        private Visibility _visibleSoundBtnInput = Visibility.Collapsed;
        public Visibility VisibleSoundBtnInput
        {
            get { return _visibleSoundBtnInput; }
            set
            {
                if (value != _visibleSoundBtnInput)
                {
                    _visibleSoundBtnInput = value;
                    NotifyPropertyChanged("VisibleSoundBtnInput");
                }
            }
        }

        private Visibility _visibleSoundBtnOutput = Visibility.Collapsed;
        public Visibility VisibleSoundBtnOutput
        {
            get { return _visibleSoundBtnOutput; }
            set
            {
                if (value != _visibleSoundBtnOutput)
                {
                    _visibleSoundBtnOutput = value;
                    NotifyPropertyChanged("VisibleSoundBtnOutput");
                }
            }
        }

        private ICommand _soundBtnInputClick;
        public ICommand SoundBtnInputClick
        {
            get
            {
                return _soundBtnInputClick ?? (_soundBtnInputClick = new RelayCommand(async (arg) =>
                {
                    if (TextBox1 != null)
                    {
                        await _speakingClass.SpeakWord(TextBox1);
                    }
                }));
            }
        }

        private ICommand _soundBtnOutputClick;
        public ICommand SoundBtnOutputClick
        {
            get
            {
                return _soundBtnOutputClick ?? (_soundBtnOutputClick = new RelayCommand(async (arg) =>
                {
                    if (TextBox2 != null)
                    {
                        await _speakingClass.SpeakWord(TextBox2);
                    }
                }));
            }
        }

        private ICommand _soundBtnOther1Click;
        public ICommand SoundBtnOther1Click
        {
            get
            {
                return _soundBtnOther1Click ?? (_soundBtnOther1Click = new RelayCommand(async (arg) =>
                {
                    if (TextBox4 != null)
                    {
                        await _speakingClass.SpeakWord(TextBox4);
                    }
                }));
            }
        }

        private ICommand _soundBtnOther2Click;
        public ICommand SoundBtnOther2Click
        {
            get
            {
                return _soundBtnOther2Click ?? (_soundBtnOther2Click = new RelayCommand(async (arg) =>
                {
                    if (TextBox5 != null)
                    {
                        await _speakingClass.SpeakWord(TextBox5);
                    }
                }));
            }
        }

        private Visibility _yandexRefVisibility = Visibility.Visible;
        public Visibility YandexRefVisibility
        {
            get { return _yandexRefVisibility; }
            set
            {
                if (value != _yandexRefVisibility)
                {
                    _yandexRefVisibility = value;
                    NotifyPropertyChanged("YandexRefVisibility");
                }
            }
        }

        private ICommand _yandexRefCommand;
        public ICommand YandexRefCommand
        {
            get
            {
                return _yandexRefCommand ?? (_yandexRefCommand = new RelayCommand((arg) =>
                {
                    System.Diagnostics.Process.Start("https://translate.yandex.ru");
                }));
            }
        }




    }
}
