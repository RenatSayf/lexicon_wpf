﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lexicon.Classes;

namespace Lexicon.Model
{
    public class AddDBModel
    {
        private SQLiteDataBaseClass _sqliteDataBaseClass;
        
        //-----------------------------------------------------------------------------------------
        public AddDBModel()
        {
            _sqliteDataBaseClass = new SQLiteDataBaseClass();
        }
        //-----------------------------------------------------------------------------------------
        public bool CheckingTableStructure(DataView table)
        {
            bool res = true;
            try
            {
                for (int i = 0; i < table.Table.Columns.Count; i++)
                {
                    res = table.Table.Columns[i].ColumnName.Equals(_sqliteDataBaseClass.StructTableDB[i]);
                    if (!res) { res = false; break; }
                }
            }
            catch (Exception) { res = false; }
            return res;
        }
        //-----------------------------------------------------------------------------------------
        public ObservableCollection<CollectionWords_Model> SelectWordFromOtherTable(DataView tableFromDb, int index)
        {
            ObservableCollection<CollectionWords_Model> tableWord = new ObservableCollection<CollectionWords_Model>();
            tableWord.Clear();
            tableWord.Add(new CollectionWords_Model());
            tableWord[0].English = tableFromDb[index].Row.ItemArray[0].ToString();
            tableWord[0].Translation = tableFromDb[index].Row.ItemArray[1].ToString();
            tableWord[0].Noun = tableFromDb[index].Row.ItemArray[2].ToString();
            tableWord[0].Verb = tableFromDb[index].Row.ItemArray[3].ToString();
            tableWord[0].Adjective = tableFromDb[index].Row.ItemArray[4].ToString();
            tableWord[0].Adverb = tableFromDb[index].Row.ItemArray[5].ToString();
            tableWord[0].Pronoun = tableFromDb[index].Row.ItemArray[6].ToString();
            tableWord[0].Preposition = tableFromDb[index].Row.ItemArray[7].ToString();
            return tableWord;
        }
        //-----------------------------------------------------------------------------------------
        public ObservableCollection<CollectionWords_Model> SelectAllWordFromOtherTable(DataView tableFromDb)
        {
            ObservableCollection<CollectionWords_Model> tableWord = new ObservableCollection<CollectionWords_Model>();
            //CollectionWords_Model rowWord = new CollectionWords_Model();

            for (int i = 0; i < tableFromDb.Count; i++)
            {
                CollectionWords_Model rowWord = new CollectionWords_Model();
                rowWord.English = tableFromDb.Table.Rows[i].ItemArray[0].ToString();
                rowWord.Translation = tableFromDb.Table.Rows[i].ItemArray[1].ToString();
                rowWord.Noun = tableFromDb.Table.Rows[i].ItemArray[2].ToString();
                rowWord.Verb = tableFromDb.Table.Rows[i].ItemArray[3].ToString();
                rowWord.Adjective = tableFromDb.Table.Rows[i].ItemArray[4].ToString();
                rowWord.Adverb = tableFromDb.Table.Rows[i].ItemArray[5].ToString();
                rowWord.Pronoun = tableFromDb.Table.Rows[i].ItemArray[6].ToString();
                rowWord.Preposition = tableFromDb.Table.Rows[i].ItemArray[7].ToString();
                tableWord.Add(rowWord);
            }
            return tableWord;
        }
        //-----------------------------------------------------------------------------------------





    }
}
