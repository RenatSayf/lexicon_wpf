﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lexicon.Classes;

namespace Lexicon
{
    /// <summary>
    /// Логика взаимодействия для PlayListWindow.xaml
    /// </summary>
    public partial class PlayListWindow : Window
    {
        private PlayListClass _playListClass;
        List<string> _list;
        //-----------------------------------------------------------------------------------------
        public PlayListWindow()
        {
            InitializeComponent();
            _playListClass = new PlayListClass();
            _list = new List<string>();
        }
        //-----------------------------------------------------------------------------------------
        private void playListWindow_Deactivated(object sender, EventArgs e)
        {
            try
            {
                playListWindow.Close();
            }
            catch { }
        }
        //-----------------------------------------------------------------------------------------
        private void lbxListDict_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _playListClass.SelectItemFromDict(lbxListDict, lbxPlayList);
        }
        //-----------------------------------------------------------------------------------------
        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            playListWindow.Close();
        }
        //-----------------------------------------------------------------------------------------
        private void btnUp_Click(object sender, RoutedEventArgs e)
        {
            _playListClass.ItemUp(lbxPlayList);
        }
        //-----------------------------------------------------------------------------------------
        private void lbxPlayList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbxPlayList.Items.Count > 1)
            {
                if (lbxPlayList.Items.IndexOf(lbxPlayList.SelectedItem) != 0)
                {
                    btnUp.IsEnabled = true;
                }
                if (lbxPlayList.Items.IndexOf(lbxPlayList.SelectedItem) != lbxPlayList.Items.Count - 1)
                {
                    btnDown.IsEnabled = true;
                }
            }
            btnDelOne.IsEnabled = true;
            if (lbxPlayList.SelectedIndex == 0)
            {
                btnUp.IsEnabled = false;
            }
            else if (lbxPlayList.SelectedIndex == lbxPlayList.Items.Count - 1)
            {
                btnDown.IsEnabled = false;
            }
        }
        //-----------------------------------------------------------------------------------------
        private void lbxPlayList_LostFocus(object sender, RoutedEventArgs e)
        {
            //btnUp.IsEnabled = false;
            //btnDown.IsEnabled = false;
            //btnDelOne.IsEnabled = false;
        }
        //-----------------------------------------------------------------------------------------
        private void btnDown_Click(object sender, RoutedEventArgs e)
        {
            _playListClass.ItemDown(lbxPlayList);
        }
        //-----------------------------------------------------------------------------------------
        private void btnDelAll_Click(object sender, RoutedEventArgs e)
        {
            _playListClass.СleansePlaylist(lbxPlayList);
        }
        //-----------------------------------------------------------------------------------------
        private void btnDelOne_Click(object sender, RoutedEventArgs e)
        {
            lbxPlayList.ItemsSource = _playListClass.DelOneItemFromPL(lbxPlayList, lbxPlayList.SelectedIndex);
            _playListClass.SavePlayList(lbxPlayList);
        }
        //-----------------------------------------------------------------------------------------
        private void playListWindow_Loaded(object sender, RoutedEventArgs e)
        {
            //cbxOrderWords.SelectedIndex = Properties.Settings.Default.playBackOrder;
            _playListClass = new PlayListClass();
        }
        //-----------------------------------------------------------------------------------------
        




    }
}
