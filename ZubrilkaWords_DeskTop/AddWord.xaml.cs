﻿using CsQuery;
using MSHTML;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using Lexicon.Classes;
using Lexicon.Model;
using Lexicon.ViewModel;
using HtmlAgilityPack;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Lexicon
{
    /// <summary>
    /// Логика взаимодействия для AddWord.xaml
    /// </summary>
    public partial class AddWord : Window
    {
        private TranslatorModel _translatorModel;
        private SQLiteDataBaseClass _sqLiteDataBaseClass;
        private SpeakingClass _speakingClass;
        private MainWindowViewModel _mainWinViewModel;
        //-----------------------------------------------------------------------------------------
        public AddWord()
        {
            InitializeComponent();
            _mainWinViewModel = new MainWindowViewModel();
            _translatorModel = new TranslatorModel();
            _sqLiteDataBaseClass = new SQLiteDataBaseClass();
            DataContext = _translatorModel;
            _listTextBlockEn = new List<TextBlock> { TxtBlockEn0, TxtBlockEn1, TxtBlockEn2, TxtBlockEn3, TxtBlockEn4 };
            _listTextBlockRu = new List<TextBlock> { TxtBlockRu0, TxtBlockRu1, TxtBlockRu2, TxtBlockRu3, TxtBlockRu4 };
            _listButtonEn = new List<Button> { BtnEn0, BtnEn1, BtnEn2, BtnEn3, BtnEn4 };
            _listButtonRu = new List<Button> { BtnRu0, BtnRu1, BtnRu2, BtnRu3, BtnRu4 };
            _checkYourSelfClass = new CheckYourSelfClass(_listTextBlockEn, _listTextBlockRu, _listButtonEn, _listButtonRu);
            _speakingClass = new SpeakingClass(null, null);
            NewWord = new ObservableCollection<CollectionWords_Model>();
            dataGridNewWord.Visibility = Visibility.Collapsed;
        }
        //-----------------------------------------------------------------------------------------
        private void Add_Word_Loaded(object sender, RoutedEventArgs e)
        {

        }
        //-----------------------------------------------------------------------------------------
        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        //-----------------------------------------------------------------------------------------
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnCanselCreateDict_Click(object sender, RoutedEventArgs e)
        {
            Add_Word.Close();
        }
        //-----------------------------------------------------------------------------------------
        private void btnCloseAddWord_Click(object sender, RoutedEventArgs e)
        {
            webBrowser = null;
            Add_Word.Close();
        }
        //-----------------------------------------------------------------------------------------
        private void btnCanselAddWord_Click(object sender, RoutedEventArgs e)
        {
            Add_Word.Close();
        }
        //-----------------------------------------------------------------------------------------
        private void dataGridNewWord_KeyUp(object sender, KeyEventArgs e)
        {
            labelWordIsAdded.Visibility = Visibility.Collapsed;
            btnAddWord.IsEnabled = true;
            if (NewWord[0].English == null || NewWord[0].English == "" && _inputText == null)
            {
                btnAddWord.IsEnabled = false;
            }
            //_addWord_Class.SetBrachCell(dataGridNewWord.Items.CurrentPosition, 0, dataGridNewWord, _table);
        }
        //-----------------------------------------------------------------------------------------
        private void CreateDict_Click(object sender, RoutedEventArgs e)
        {
            _comBox = comBox;
            tabAddRemoveDict.Focus();
        }
        //-----------------------------------------------------------------------------------------
        private async void comBox_DropDownOpened(object sender, EventArgs e)
        {
            comBox.ItemsSource = await MainWindow._sqLiteDataBaseClass.GetListTable();
        }
        //-----------------------------------------------------------------------------------------
        private static ComboBox _comBox;
        public static void SetSelectItem(string text)
        {
            _comBox.Text = text;
        }
        //-----------------------------------------------------------------------------------------
        private void textBox2_GotFocus(object sender, RoutedEventArgs e)
        {
            textBox2.Text = "";
        }
        //-----------------------------------------------------------------------------------------
        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {
            string text = textBox2.Text;
            char[] charStr = new char[text.Length];
            if (text.Length > 0)
            {
                charStr = text.ToCharArray();
                if (char.IsLetter(charStr[0]))
                {
                    btnAddDict.IsEnabled = true;
                }
                else
                {
                    btnAddDict.IsEnabled = false;
                }
            }
            else
            {
                btnAddDict.IsEnabled = false;
            }
        }
        //-----------------------------------------------------------------------------------------
        private async void comBox2_DropDownOpened(object sender, EventArgs e)
        {
            comBox2.ItemsSource = await MainWindow._sqLiteDataBaseClass.GetListTable();
        }
        //-----------------------------------------------------------------------------------------
        private void comBox2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //btnRemoveDict.IsEnabled = true;
        }
        //-----------------------------------------------------------------------------------------
        private void btnRemoveDict_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MainWindow._sqLiteDataBaseClass.RemoveTable(comBox2.SelectedItem.ToString()) == 0)
                {
                    MessageBox.Show("Dictionary successfully deleted");
                    //comBox2.Text = "Select";
                    //btnRemoveDict.IsEnabled = false;
                }
            }
            catch (Exception err)
            {
                Debug.WriteLine("Событие btnRemoveDict_Click выдало исключение - \n" + err.Message);
            }
        }
        //-----------------------------------------------------------------------------------------
        private void btnRenameDict_Click(object sender, RoutedEventArgs e)
        {
            //LabelWellDone.Opacity = 100.0;
            //DoubleAnimation anim = new DoubleAnimation();
            //anim.From = LabelWellDone.Opacity;
            //anim.To = 0.0;
            //anim.Duration = TimeSpan.FromSeconds(5);
            //LabelWellDone.BeginAnimation(Label.OpacityProperty, anim);
        }
        //-----------------------------------------------------------------------------------------
        private void btnAddDict_Click(object sender, RoutedEventArgs e)
        {
            if (MainWindow._sqLiteDataBaseClass.CreateTable(textBox2.Text) == 0)
            {
                textBox2.Text = "Dictionary created";
                btnAddDict.IsEnabled = false;
                tabAddWord.Focus();
            }
        }
        //-----------------------------------------------------------------------------------------
        private void btnClose2_Click(object sender, RoutedEventArgs e)
        {
            Add_Word.Close();
        }
        //-----------------------------------------------------------------------------------------
        private void tabEditWord_Loaded(object sender, RoutedEventArgs e)
        {
            //SQLiteDataBaseClass sqlDataBaseClass = new SQLiteDataBaseClass();
            //sqlDataBaseClass.GetAllFromTable("This_Application");

        }
        //-----------------------------------------------------------------------------------------
        private void combxEditWords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //var a = combxEditWords.SelectedItem;
            //listViewEditWords.ItemsSource = _sqlDataBaseClass.GetAllFromTable(a.ToString());
        }





        #region tabAddWord

        private string _url = "https://translate.google.com/";
        private HTMLDocument _HTMLdoc = new HTMLDocument();
        private string _inputText;
        public ObservableCollection<CollectionWords_Model> NewWord { get; set; }
        public CollectionWords_Model wordModel = new CollectionWords_Model();

        private void tabAddWord_Loaded(object sender, RoutedEventArgs e)
        {
            labelWordIsAdded.Visibility = Visibility.Collapsed;
            circProgressBar.Visibility = Visibility.Visible;
            wordModel.Clean();
            NewWord.Add(wordModel);
            dataGridNewWord.ItemsSource = NewWord;
            webBrowser.Navigate(_url);
        }

        public void HideScriptErrors(WebBrowser wb, bool Hide)
        {
            FieldInfo fiComWebBrowser = typeof(WebBrowser).GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fiComWebBrowser == null) return;
            object objComWebBrowser = fiComWebBrowser.GetValue(wb);
            if (objComWebBrowser == null) return;
            objComWebBrowser.GetType().InvokeMember("Silent", BindingFlags.SetProperty, null, objComWebBrowser, new object[] { Hide });

        }

        private void webBrowser_Navigating(object sender, System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            if (!e.Uri.AbsoluteUri.Equals(_url))
            {
                e.Cancel = true;
            }
        }

        private void webBrowser_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            HideScriptErrors(webBrowser, true);
        }

        private void webBrowser_LoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            try
            {
                _HTMLdoc = (HTMLDocument)webBrowser.Document;
            }
            catch (Exception err)
            {
                Debug.WriteLine(err.Message);
                return;
            }
            circProgressBar.Visibility = Visibility.Collapsed;
        }

        private void GetMainTranslate()
        {
            try
            {
                _HTMLdoc = (HTMLDocument)webBrowser.Document;
            }
            catch (Exception err)
            {
                Debug.WriteLine(err.Message);
                return;
            }
            NewWord[0].English = _HTMLdoc.getElementById("source").innerHTML;
            var result_box = _HTMLdoc.getElementById("result_box").innerHTML;
            HtmlDocument htmlDoc1 = new HtmlDocument();
            try
            {
                if (result_box != null)
                {
                    htmlDoc1.LoadHtml(result_box);
                    NewWord[0].Translation = htmlDoc1.DocumentNode.SelectNodes("//span")[0].InnerText;
                }
            }
            catch (Exception err)
            {
                Debug.WriteLine(err.Message);
                return;
            }
        }

        private void webBrowser_KeyDown(object sender, KeyEventArgs e)
        {
            WebBrowserKeyHandler();
            GetMainTranslate();
        }

        private void webBrowser_KeyUp(object sender, KeyEventArgs e)
        {
            WebBrowserKeyHandler();
            GetMainTranslate();
        }


        private async void btnAddVariants_Click(object sender, RoutedEventArgs e)
        {
            dataGridNewWord.Visibility = Visibility.Visible;
            circProgressBar.Visibility = Visibility.Visible;
            NewWord[0].Clean();
            dataGridNewWord.ItemsSource = null;

            _HTMLdoc = (HTMLDocument)webBrowser.Document;

            await Task.Run(() =>
            {
                try
                {
                    NewWord[0].English = _HTMLdoc.getElementById("source").innerHTML;
                    var result_box = _HTMLdoc.getElementById("result_box").innerHTML;
                    HtmlDocument htmlDoc1 = new HtmlDocument();
                    htmlDoc1.LoadHtml(result_box);
                    NewWord[0].Translation = htmlDoc1.DocumentNode.SelectNodes("//span")[0].InnerText;

                    var htmlContent = _HTMLdoc.getElementById("gt-lc").innerHTML;
                    HtmlDocument htmlDoc = new HtmlDocument();
                    htmlDoc.LoadHtml(htmlContent);

                    //var dom = CQ.Create(htmlContent);
                    //var el = dom.Select(".gt-baf-marker-container")[0].GetAttribute("title");
                    //var span_word = dom.Select(".gt-baf-word-clickable")[0].InnerText;


                    string marker = null;
                    string marker2 = null;
                    for (int i = 0; i < 33; i++)
                    {
                        try
                        {

                            marker = htmlDoc.DocumentNode.SelectSingleNode("//div[2]/div[2]/div/div/div[2]/table/tbody/tr[" + i + "]/td/div/span").InnerText;
                            marker2 = htmlDoc.DocumentNode.SelectSingleNode("//div[2]/div[2]/div/div/div[2]/table/tbody/tr[" + (i + 1) + "]/td[1]/div").GetAttributeValue("title", null);
                        }
                        catch (Exception)
                        {
                            continue;
                        }
                        if (marker == "имя существительное" && marker2 == "Обычный перевод")
                        {
                            NewWord[0].Noun = htmlDoc.DocumentNode.SelectSingleNode("//div[2]/div[2]/div/div/div[2]/table/tbody/tr[" + (i + 1) + "]/td[2]/div/span").InnerText;
                        }
                        if (marker == "имя прилагательное" && marker2 == "Обычный перевод")
                        {
                            NewWord[0].Adjective = htmlDoc.DocumentNode.SelectSingleNode("//div[2]/div[2]/div/div/div[2]/table/tbody/tr[" + (i + 1) + "]/td[2]/div/span").InnerText;
                        }
                        if (marker == "глагол" && marker2 == "Обычный перевод")
                        {
                            NewWord[0].Verb = htmlDoc.DocumentNode.SelectSingleNode("//div[2]/div[2]/div/div/div[2]/table/tbody/tr[" + (i + 1) + "]/td[2]/div/span").InnerText;
                        }
                        if (marker == "наречие" && marker2 == "Обычный перевод")
                        {
                            NewWord[0].Adverb = htmlDoc.DocumentNode.SelectSingleNode("//div[2]/div[2]/div/div/div[2]/table/tbody/tr[" + (i + 1) + "]/td[2]/div/span").InnerText;
                        }
                        if (marker == "местоимение" && marker2 == "Обычный перевод")
                        {
                            NewWord[0].Pronoun = htmlDoc.DocumentNode.SelectSingleNode("//div[2]/div[2]/div/div/div[2]/table/tbody/tr[" + (i + 1) + "]/td[2]/div/span").InnerText;
                        }
                        if (marker == "предлог" && marker2 == "Обычный перевод")
                        {
                            NewWord[0].Preposition = htmlDoc.DocumentNode.SelectSingleNode("//div[2]/div[2]/div/div/div[2]/table/tbody/tr[" + (i + 1) + "]/td[2]/div/span").InnerText;
                        }
                    }
                }
                catch (Exception err)
                {
                    Debug.WriteLine("Исключение в btnAddVariants_Click" + err.Message);
                }

            });

            if (NewWord == null) return;
            NewWord[0].DoubleDelete();
            if (NewWord[0].English != null && NewWord[0].Translation != null)
            {
                if (_inputText.Any(wordByte => wordByte >= 122 && wordByte >= 65) /*&& NewWord[0].English.Any(wordByte => wordByte > 122 && wordByte >= 65)*/)
                {
                    string temp = NewWord[0].English;
                    NewWord[0].English = NewWord[0].Translation;
                    NewWord[0].Translation = temp;
                    NewWord[0].Noun = ""; NewWord[0].Verb = ""; NewWord[0].Adjective = ""; NewWord[0].Adverb = ""; NewWord[0].Pronoun = ""; NewWord[0].Preposition = "";
                }
                //else if (NewWord[0].Translation.Any(wordByte => wordByte <= 122 && wordByte >= 65) && NewWord[0].English.Any(wordByte => wordByte < 122 && wordByte >= 65))
                //{
                //    return;
                //}
                //else if (NewWord[0].Translation.Any(wordByte => wordByte > 122) && NewWord[0].English.Any(wordByte => wordByte > 122))
                //{
                //    return;
                //}
            }
            dataGridNewWord.ItemsSource = NewWord;
            circProgressBar.Visibility = Visibility.Collapsed;
        }

        private async void WebBrowserKeyHandler()
        {
            await Task.Run(() =>
            {
                try
                {
                    _inputText = _HTMLdoc.getElementById("source").innerText;
                }
                catch (Exception)
                {
                    _inputText = null;
                }
            });

            if (_inputText == null || NewWord[0].English != _inputText)
            {
                dataGridNewWord.Visibility = Visibility.Collapsed;
                btnAddWord.IsEnabled = false;
            }
            else
            {
                btnAddWord.IsEnabled = true;
            }
            labelWordIsAdded.Visibility = Visibility.Collapsed;
        }

        private async void btnAddWord_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _inputText = _HTMLdoc.getElementById("source").innerText;
            }
            catch (Exception)
            {
                _inputText = null;
                return;
            }
            //if (_inputText != NewWord[0].English || _inputText == null)
            //{
            //    return;
            //}
            if (comBox.SelectedItem != null && NewWord.Count > 0)
            {
                if (_sqLiteDataBaseClass.AddWordInTable(comBox.SelectedItem.ToString(), NewWord) > 0)
                {
                    foreach (var item in NewWord)
                    {
                        item.English = ""; item.Translation = ""; item.Noun = ""; item.Verb = ""; item.Adjective = ""; item.Adverb = ""; item.Pronoun = ""; item.Preposition = "";
                    }
                    labelWordIsAdded.Visibility = Visibility.Visible;
                    for (double i = 1; i >= 0; i = i - 0.1)
                    {
                        await Task.Delay(200);
                        labelWordIsAdded.Opacity = i; 
                    }
                    labelWordIsAdded.Visibility = Visibility.Collapsed;
                }
            }
            else
            {
                comBox.IsDropDownOpen = true;
            }
        }
        #endregion




        #region TabCheckYourSelf
        //-----------------------------------------------------------------------------------------
        private CheckYourSelfClass _checkYourSelfClass;
        private List<TextBlock> _listTextBlockEn;
        private List<TextBlock> _listTextBlockRu;
        private List<Button> _listButtonEn;
        private List<Button> _listButtonRu;

        private TextBlock _textBlockEn;// = new TextBlock();
        private TextBlock _textBlockRu;// = new TextBlock();
        private Button _buttonEn;// = new Button();
        private Button _buttonRu;// = new Button();
        private int _countCorrectAnswersOnList = 1;
        
        //-----------------------------------------------------------------------------------------
        private async void TabCheckYourSelf_Loaded(object sender, RoutedEventArgs e)
        {
            _textBlockEn = new TextBlock();
            _textBlockRu = new TextBlock();
            _buttonEn = new Button();
            _buttonRu = new Button();
            if (Properties.Settings.Default.settingsPlayList.Count > 0)
            {
                await _checkYourSelfClass.SetItemsSourseComboBox(ComboxTab4);
            }
            ComboxTab4.SelectionChanged += ComboxTab4_SelectionChanged;
            LabelProgressBar.Content = string.Format("Правильных ответов {0} из {1}", _checkYourSelfClass.GetCorrectAnswersTotal, _checkYourSelfClass.GetNumberRecords);
            ProgressBar.Minimum = 0.0;
            ProgressBar.Maximum = _checkYourSelfClass.GetNumberRecords;
        }
        //-----------------------------------------------------------------------------------------
        private async void ComboxTab4_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _buttonEn = new Button();
            _textBlockEn = new TextBlock();
            _buttonRu = new Button();
            _textBlockRu = new TextBlock();
            _countCorrectAnswersOnList = 1;
            await _checkYourSelfClass.GetWordsFromNextDict(ComboxTab4.SelectedItem.ToString());
            LabelProgressBar.Content = string.Format("Правильных ответов {0} из {1}", _checkYourSelfClass.GetCorrectAnswersTotal, _checkYourSelfClass.GetNumberRecords);
            ProgressBar.Minimum = 0.0;
            ProgressBar.Maximum = _checkYourSelfClass.GetNumberRecords;
            ProgressBar.Value = 0.0;
            BtnRepeat.IsEnabled = false;
        }

        //-----------------------------------------------------------------------------------------
        private async void BtnEn0_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in _listButtonEn)
            {
                item.IsEnabled = false;
            }
            _buttonEn = sender as Button;
            _textBlockEn = (TextBlock)_buttonEn.Content;
            _buttonEn.Background = Brushes.LawnGreen;
            if (_countCorrectAnswersOnList > _listButtonEn.Count)
            {
                _buttonRu = new Button();
                _textBlockRu = new TextBlock();
                _countCorrectAnswersOnList = 1;
            }
            int res = await _checkYourSelfClass.VerifyOfCompliance(_textBlockEn, _buttonEn, _textBlockRu, _buttonRu);
            if (res < 0)
            {
                _buttonEn.Background = Brushes.Red;
                foreach (var item in _listButtonEn)
                {
                    TextBlock t = (TextBlock)item.Content;
                    if (t.Text != "")
                    {
                        item.IsEnabled = true;
                    }
                }
            }
            else if (res > 0)
            {
                _countCorrectAnswersOnList++;
            }
            LabelProgressBar.Content = string.Format("{0} / {1}", _checkYourSelfClass.GetCorrectAnswersTotal, _checkYourSelfClass.GetNumberRecords);
            ProgressBar.Value = _checkYourSelfClass.GetCorrectAnswersTotal;
            BtnRepeat.IsEnabled = _checkYourSelfClass.TrainingListIsUpdate;
        }
        //-----------------------------------------------------------------------------------------
        private async void BtnRu0_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in _listButtonRu)
            {
                item.IsEnabled = false;
            }
            _buttonRu = sender as Button;
            _textBlockRu = (TextBlock)_buttonRu.Content;
            _buttonRu.Background = Brushes.LawnGreen;
            if (_countCorrectAnswersOnList > _listButtonRu.Count)
            {
                _buttonEn = new Button();
                _textBlockEn = new TextBlock();
                _countCorrectAnswersOnList = 1;
            }
            int res = await _checkYourSelfClass.VerifyOfCompliance(_textBlockEn, _buttonEn, _textBlockRu, _buttonRu);
            if (res < 0)
            {
                _buttonRu.Background = Brushes.Red;
                foreach (var item in _listButtonRu)
                {
                    TextBlock t = (TextBlock)item.Content;
                    if (t.Text != "")
                    {
                        item.IsEnabled = true;
                    }
                }
            }
            else if (res > 0)
            {
                _countCorrectAnswersOnList++;
            }
            LabelProgressBar.Content = string.Format("{0} / {1}", _checkYourSelfClass.GetCorrectAnswersTotal, _checkYourSelfClass.GetNumberRecords);
            ProgressBar.Value = _checkYourSelfClass.GetCorrectAnswersTotal;
            BtnRepeat.IsEnabled = _checkYourSelfClass.TrainingListIsUpdate;
        }
        //-----------------------------------------------------------------------------------------
        private void BtnEn0_MouseEnter(object sender, MouseEventArgs e)
        {
            Button button = sender as Button;
            if (button != null && !Equals(button.Background, Brushes.Red))
            {
                button.Background = Brushes.LightBlue;
            }
        }
        //-----------------------------------------------------------------------------------------
        private void BtnEn0_MouseLeave(object sender, MouseEventArgs e)
        {
            Button button = sender as Button;
            if (button != null && Equals(button.Background, Brushes.LightBlue))
            {
                button.Background = Brushes.Transparent;
            }
        }

        //-----------------------------------------------------------------------------------------
        private void ButtonTab4_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button.Name == "BtnRepeat")
            {
                ComboxTab4_SelectionChanged(ComboxTab4, null);
            }
            if (button.Name == "BtnClose")
            {
                Close();
            }
        }




        //-----------------------------------------------------------------------------------------
        #endregion


        #region TabEditWord
        //-----------------------------------------------------------------------------------------
        private void dataGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            var index = dataGrid.SelectedIndex;
            var item = dataGrid.SelectedItems;
            var a = item;
            
        }
        //-----------------------------------------------------------------------------------------
        private void dataGridTabEW2_KeyUp(object sender, KeyEventArgs e)
        {
            BtnUpdate.IsEnabled = true;
        }

        //---------------------------------------------------------------------------------------
        #endregion




        #region Closing
        //-----------------------------------------------------------------------------------------
        private void Add_Word_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (webBrowser != null) webBrowser.Dispose();
            if (MainWindow.winAddWord != null) MainWindow.winAddWord = null;
        }

        
        #endregion










        //---------------------------------------------------------------------------------------

    }
}
