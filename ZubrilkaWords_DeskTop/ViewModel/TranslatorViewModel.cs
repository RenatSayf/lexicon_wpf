﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lexicon.Model;

namespace Lexicon.ViewModel
{
    public class TranslatorViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<CollectionWords_Model> NewWord { get; set; }
        private TranslatorModel _translatorModel;
        //-----------------------------------------------------------------------------------------
        public TranslatorViewModel()
        {
            _translatorModel = new TranslatorModel();
            NewWord = new ObservableCollection<CollectionWords_Model>();
        }
        //-----------------------------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        //-----------------------------------------------------------------------------------------
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        //-----------------------------------------------------------------------------------------
        private bool _btnAddWordIsEnable;
        public bool BtnAddWordIsEnable
        {
            get { return _btnAddWordIsEnable; }
            set
            {
                if (value != this._btnAddWordIsEnable)
                {
                    this._btnAddWordIsEnable = value;
                    NotifyPropertyChanged("BtnAddWordIsEnable");
                }
            }
           
        }
        //-----------------------------------------------------------------------------------------







    }
}
