﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Lexicon.Classes
{
    public class CheckYourSelfClass : INotifyPropertyChanged
    {
        private ComboBox _comboBox;
        private PlayListClass _playListClass;
        private SQLiteDataBaseClass _sqlLiteDataBaseClass;
        private List<TextBlock> _listTxtBlockEn;
        private List<TextBlock> _listTxtBlockRu;
        private List<Button> _listButtonEn;
        private List<Button> _listButtonRu;
        private DataView _etalonTable;
        private List<string> _etalonList;
        private SpeakingClass _speakingClass;
        private int _numberRecords = 0;
        private int _wordCounter = 0;
        private Random _random;
        //-----------------------------------------------------------------------------------------
        public CheckYourSelfClass()
        {

        }
        //-----------------------------------------------------------------------------------------
        public CheckYourSelfClass(List<TextBlock> txtBlockEn, List<TextBlock> txtBlockRu, List<Button> listButtonEn, List<Button> listButtonRu)
        {
            _playListClass = new PlayListClass();
            _sqlLiteDataBaseClass = new SQLiteDataBaseClass();
            _speakingClass = new SpeakingClass(null, null);
            _listTxtBlockEn = txtBlockEn;
            _listTxtBlockRu = txtBlockRu;
            _listButtonEn = listButtonEn;
            _listButtonRu = listButtonRu;
            _etalonList = new List<string>();
            _random = new Random();
        }
        //-----------------------------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        //-----------------------------------------------------------------------------------------
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        //-----------------------------------------------------------------------------------------
        public async Task SetItemsSourseComboBox(ComboBox comBox)
        {
            _сorrectAnswersTotal = 0;
            _comboBox = comBox;
            _comboBox.ItemsSource = null;
            _comboBox.ItemsSource = await _sqlLiteDataBaseClass.GetListTable();
            //comBox.ItemsSource = _playListClass.GetPlayList();
            await Task.Delay(10); 
            if (Data.Ndict > -1 && _comboBox.Items.Contains(Data.CurrentNameDict))
            {
                //_comboBox.SelectedIndex = Data.Ndict;
                _comboBox.SelectedItem = Data.CurrentNameDict;
            }
            else if (_comboBox.Items.Count > 0)
            {
                _comboBox.SelectedIndex = 0;
            }
            _numberRecords = await _sqlLiteDataBaseClass.NumberRecords(_comboBox.SelectedItem.ToString());
            _etalonTable = await _sqlLiteDataBaseClass.GetNextFiveWosdsTable(_comboBox.SelectedItem.ToString(), 1, 5);
            ListTextBlockEn();
            ListTextBlockRu();
            //_etalonList = ConvertTableToList(_etalonTable);
            _etalonList = ConvertTableToList2(_etalonTable);
        }
        //-----------------------------------------------------------------------------------------
        //public string GetSelectItemComboBox
        //{
        //    get { return _comboBox.SelectedItem.ToString(); }
        //}
        //-----------------------------------------------------------------------------------------
        private void ListTextBlockEn()
        {
            List<string> list = new List<string>();
            for (int i = 0; i < _etalonTable.Count; i++)
            {
                if (_etalonTable[0].DataView.Table.Rows[i].ItemArray[0].ToString() != "")
                {
                    //_listTxtBlockEn[i].Text = _etalonTable[0].DataView.Table.Rows[i].ItemArray[0].ToString();
                    list.Add(_etalonTable[0].DataView.Table.Rows[i].ItemArray[0].ToString());
                }
            }
            list = list.OrderBy(x => _random.Next()).ToList();
            for (int i = 0; i < list.Count; i++)
            {
                _listTxtBlockEn[i].Text = list[i];
            }
        }
        //-----------------------------------------------------------------------------------------
        private void ListTextBlockRu()
        {
            List<string> list = new List<string>();
            for (int i = 0; i < _etalonTable.Count; i++)
            {
                string text = "";
                if (_etalonTable[i].DataView.Table.Rows[i].ItemArray[1].ToString() != "") text = _etalonTable[i].DataView.Table.Rows[i].ItemArray[1].ToString();
                if (_etalonTable[i].DataView.Table.Rows[i].ItemArray[2].ToString() != "") text += ", " + _etalonTable[i].DataView.Table.Rows[i].ItemArray[2].ToString();
                if (_etalonTable[i].DataView.Table.Rows[i].ItemArray[3].ToString() != "") text += ", " + _etalonTable[i].DataView.Table.Rows[i].ItemArray[3].ToString();
                if (_etalonTable[i].DataView.Table.Rows[i].ItemArray[4].ToString() != "") text += ", " + _etalonTable[i].DataView.Table.Rows[i].ItemArray[4].ToString();
                if (_etalonTable[i].DataView.Table.Rows[i].ItemArray[5].ToString() != "") text += ", " + _etalonTable[i].DataView.Table.Rows[i].ItemArray[5].ToString();
                if (_etalonTable[i].DataView.Table.Rows[i].ItemArray[6].ToString() != "") text += ", " + _etalonTable[i].DataView.Table.Rows[i].ItemArray[6].ToString();
                if (_etalonTable[i].DataView.Table.Rows[i].ItemArray[7].ToString() != "") text += ", " + _etalonTable[i].DataView.Table.Rows[i].ItemArray[7].ToString();

                text = text.Trim(',');
                list.Add(text);
            }
            list = list.OrderBy(x => _random.Next()).ToList();
            for (int i = 0; i < list.Count; i++)
            {
                _listTxtBlockRu[i].Text = list[i];
            }
        }
        //-----------------------------------------------------------------------------------------
        private List<string> ConvertTableToList(DataView table)
        {
            string textEn = "", textRu = "";
            List<string> list = new List<string>();
            for (int i = 0; i < table.Count; i++)
            {
                if (table[i].DataView.Table.Rows[i].ItemArray[0].ToString() != "") textEn = table[i].DataView.Table.Rows[i].ItemArray[0].ToString();
                textEn = textEn.Trim(',');
                textEn = textEn.Replace(",", "");
                textEn = textEn.Replace(" ", "");
                if (table[i].DataView.Table.Rows[i].ItemArray[1].ToString() != "") textRu += ", " + table[i].DataView.Table.Rows[i].ItemArray[1].ToString();
                textRu = textRu.Trim(',');
                textRu = textRu.Replace(",", "");
                textRu = textRu.Replace(" ", "");
                if (table[i].DataView.Table.Rows[i].ItemArray[2].ToString() != "") textRu += ", " + table[i].DataView.Table.Rows[i].ItemArray[2].ToString();
                textRu = textRu.Trim(',');
                textRu = textRu.Replace(",", "");
                textRu = textRu.Replace(" ", "");
                if (table[i].DataView.Table.Rows[i].ItemArray[3].ToString() != "") textRu += ", " + table[i].DataView.Table.Rows[i].ItemArray[3].ToString();
                textRu = textRu.Trim(',');
                textRu = textRu.Replace(",", "");
                textRu = textRu.Replace(" ", "");
                if (table[i].DataView.Table.Rows[i].ItemArray[4].ToString() != "") textRu += ", " + table[i].DataView.Table.Rows[i].ItemArray[4].ToString();
                textRu = textRu.Trim(',');
                textRu = textRu.Replace(",", "");
                textRu = textRu.Replace(" ", "");
                if (table[i].DataView.Table.Rows[i].ItemArray[5].ToString() != "") textRu += ", " + table[i].DataView.Table.Rows[i].ItemArray[5].ToString();
                textRu = textRu.Trim(',');
                textRu = textRu.Replace(",", "");
                textRu = textRu.Replace(" ", "");
                if (table[i].DataView.Table.Rows[i].ItemArray[6].ToString() != "") textRu += ", " + table[i].DataView.Table.Rows[i].ItemArray[6].ToString();
                textRu = textRu.Trim(',');
                textRu = textRu.Replace(",", "");
                textRu = textRu.Replace(" ", "");
                if (table[i].DataView.Table.Rows[i].ItemArray[7].ToString() != "") textRu += ", " + table[i].DataView.Table.Rows[i].ItemArray[7].ToString();
                textRu = textRu.Trim(',');
                textRu = textRu.Replace(",", "");
                textRu = textRu.Replace(" ", "");
                //textRu = textRu.Trim(',');
                string text = textEn + textRu;
                text = text.Trim(',');
                //text = text.Replace(",", "");
                //text = text.Replace(" ", "");
                list.Add(text);
                textEn = "";
                textRu = "";
            }
            return list;
        }
        //-----------------------------------------------------------------------------------------
        private List<string> ConvertTableToList2(DataView table)
        {
            string textEn = "", textRu = "";
            List<string> list = new List<string>();
            for (int i = 0; i < table.Count; i++)
            {
                if (table[i].DataView.Table.Rows[i].ItemArray[0].ToString() != "") textEn = table[i].DataView.Table.Rows[i].ItemArray[0].ToString() + " ";

                if (table[i].DataView.Table.Rows[i].ItemArray[1].ToString() != "") textRu += table[i].DataView.Table.Rows[i].ItemArray[1].ToString() + ", ";

                if (table[i].DataView.Table.Rows[i].ItemArray[2].ToString() != "") textRu += table[i].DataView.Table.Rows[i].ItemArray[2].ToString() + ", ";

                if (table[i].DataView.Table.Rows[i].ItemArray[3].ToString() != "") textRu += table[i].DataView.Table.Rows[i].ItemArray[3].ToString() + ", ";

                if (table[i].DataView.Table.Rows[i].ItemArray[4].ToString() != "") textRu += table[i].DataView.Table.Rows[i].ItemArray[4].ToString() + ", ";

                if (table[i].DataView.Table.Rows[i].ItemArray[5].ToString() != "") textRu += table[i].DataView.Table.Rows[i].ItemArray[5].ToString() + ", ";

                if (table[i].DataView.Table.Rows[i].ItemArray[6].ToString() != "") textRu += table[i].DataView.Table.Rows[i].ItemArray[6].ToString() + ", ";

                if (table[i].DataView.Table.Rows[i].ItemArray[7].ToString() != "") textRu += table[i].DataView.Table.Rows[i].ItemArray[7].ToString() + ", ";
                textEn = textEn.Trim(',');
                textEn = textEn.Trim();
                textRu = textRu.Trim(',');
                textRu = textRu.Trim();
                string text = textEn + " " + textRu;
                text = text.Trim(',');
                text = text.Trim();
                text = text.Trim(',');
                list.Add(text);
                textEn = "";
                textRu = "";
            }
            return list;
        }
        //-----------------------------------------------------------------------------------------
        private int FindMatch(TextBlock enWord, TextBlock ruWord)
        {
            int res = -1;
            _etalonList = ConvertTableToList2(_etalonTable);
            if (enWord == null || ruWord == null) return 0;
            if (enWord.Text == "" || ruWord.Text == "") return 0;
            string enword = enWord.Text;
            string ruword = ruWord.Text;
            
            bool flag = true;
            while (flag)
            {
                char a = enword.First();
                char b = enword.Last();
                if (!char.IsLetter(a) && !char.IsNumber(a))
                {
                    enword = enword.Trim(a);  continue;
                }
                if (!char.IsLetter(b) && !char.IsNumber(b))
                {
                    enword = enword.Trim(b); continue;
                }
                flag = false;
            }
            flag = true;
            while (flag)
            {
                char a = ruword.First();
                char b = ruword.Last();
                if (!char.IsLetter(a) && !char.IsNumber(a))
                {
                    ruword = ruword.Trim(a); continue;
                }
                if (!char.IsLetter(b) && !char.IsNumber(b))
                {
                    ruword = ruword.Trim(b); continue;
                }
                flag = false;
            }

            string text = enword + " " + ruword;

            for (int i = 0; i < _etalonList.Count; i++)
            {
                if (text.Equals(_etalonList[i]))
                {
                    res = 1;
                    break;
                }
            }

            return res;
        }
        //-----------------------------------------------------------------------------------------
        private TextBlock _txtBlockEn = new TextBlock();
        private TextBlock _txtBlockRu = new TextBlock();
        private Button _buttonEn = new Button();
        private Button _buttonRu = new Button();
        private int _сorrectAnswersTotal = 0;
        //-----------------------------------------------------------------------------------------
        public async Task<int> VerifyOfCompliance(TextBlock txtBlockEn, Button btnEn, TextBlock txtBlockRu, Button btnRu)
        {
            int res = -1;
            _buttonEn = btnEn;
            _buttonRu = btnRu;
            _txtBlockEn = txtBlockEn;
            _txtBlockRu = txtBlockRu;

            if (_txtBlockEn.Text == "" || _txtBlockRu.Text == "")
            {
                res = 0;
            }

            if (_txtBlockEn.Text != "" && _txtBlockRu.Text != "")
            {
                int conformity = FindMatch(txtBlockEn, txtBlockRu);
                if (conformity > 0)
                {
                    res = 1;
                    string text = txtBlockEn.Text;
                    _txtBlockEn.Text = ""; _txtBlockRu.Text = "";
                    _buttonEn.IsEnabled = false; //_buttonEn.Visibility = Visibility.Collapsed;
                    _buttonRu.IsEnabled = false; //_buttonRu.Visibility = Visibility.Collapsed;
                    _сorrectAnswersTotal++;
                    await _speakingClass.SpeakWord(text);
                    await UpdateTrainingList();
                    //await _speakingClass.SpeakWord(text);
                    foreach (var item in _listButtonEn)
                    {
                        item.Background = Brushes.Transparent;
                        item.BorderBrush = Brushes.Transparent;
                        TextBlock t = (TextBlock)item.Content;
                        if (t.Text != "")
                        {
                            item.IsEnabled = true;
                        }
                    }
                    foreach (var item in _listButtonRu)
                    {
                        item.Background = Brushes.Transparent;
                        item.BorderBrush = Brushes.Transparent;
                        TextBlock t = (TextBlock)item.Content;
                        if (t.Text != "")
                        {
                            item.IsEnabled = true;
                        }
                    }
                }
                else if (conformity < 0)
                {
                    res = -1;
                    _сorrectAnswersTotal--;
                }
            }

            return res;
        }
        //-----------------------------------------------------------------------------------------
        public async Task<bool> UpdateTrainingList()
        {
            bool res = false;
            bool res1 = _listTxtBlockEn.All(i => i.Text == "");
            bool res2 = _listTxtBlockRu.All(i => i.Text == "");

            _wordCounter++;
            if (res1 && res2 && _wordCounter <= _numberRecords)
            {
                _etalonTable = await _sqlLiteDataBaseClass.GetNextFiveWosdsTable(_comboBox.SelectedItem.ToString(), _wordCounter + 1, _wordCounter + 5);
                ListTextBlockEn();
                ListTextBlockRu();
                _etalonList = ConvertTableToList2(_etalonTable);
                res = true;
            }
            if (_wordCounter >= _numberRecords)
            {
                _trainingListIsUpdate = true;
                string textResult = string.Format("Ваш результат {0} из {1}", _сorrectAnswersTotal, _numberRecords);
                await _speakingClass.SpeakWord(textResult);
            }
            return res;
        }
        //-----------------------------------------------------------------------------------------
        public async Task GetWordsFromNextDict(string item)
        {
            _сorrectAnswersTotal = 0;
            _numberRecords = await _sqlLiteDataBaseClass.NumberRecords(item);
            for (int i = 0; i < _listTxtBlockEn.Count; i++)
            {
                _listTxtBlockEn[i].Text = "";
                _listTxtBlockRu[i].Text = "";
                _listButtonEn[i].IsEnabled = true;
                _listButtonRu[i].IsEnabled = true;
            }
            _wordCounter = 0;
            _etalonTable = await _sqlLiteDataBaseClass.GetNextFiveWosdsTable(item, 1, 5);
            ListTextBlockEn();
            ListTextBlockRu();
            _etalonList = ConvertTableToList2(_etalonTable);
        }
        //-----------------------------------------------------------------------------------------
        public int GetNumberRecords
        {
            get { return _numberRecords; }
        }
        //-----------------------------------------------------------------------------------------
        public int GetCorrectAnswersTotal
        {
            get { return _сorrectAnswersTotal; }
        }
        //-----------------------------------------------------------------------------------------
        private bool _trainingListIsUpdate;
        public bool TrainingListIsUpdate
        {
            get { return _trainingListIsUpdate; }
        }




    }
}
