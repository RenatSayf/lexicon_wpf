﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Lexicon.Classes;
using Lexicon.Model;

namespace Lexicon.ViewModel
{
    public class EditWordViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<CollectionWords_Model> EditableWord { get; set; }
        public ObservableCollection<CollectionWords_Model> EditableWord2 { get; set; }

        private SQLiteDataBaseClass _sqlDataBaseClass;
        private EditWordModel _editWordModel;
        //-----------------------------------------------------------------------------------------
        public EditWordViewModel()
        {
            _sqlDataBaseClass = new SQLiteDataBaseClass();
            _editWordModel = new EditWordModel();
            EditableWord = new ObservableCollection<CollectionWords_Model>();
            EditableWord2 = new ObservableCollection<CollectionWords_Model>();
            ComBox = Task.Run(async () => await _sqlDataBaseClass.GetListTable()).Result;

            if (Properties.Settings.Default.settingsPlayList.Count > 0 && Data.CurrentNameDict == null)
            {
                ComBox1SelectedItem = Properties.Settings.Default.settingsPlayList[0];
            }
            else if (Data.CurrentNameDict != null && Data.CurrentTableWord.Count > 0)
            {
                ComBox1SelectedItem = Data.CurrentNameDict;
                EditableWord2 = Data.CurrentTableWord;
                EditableWord = Data.CurrentTableWord;
                _rowId = Task.Run(async () => await _sqlDataBaseClass.GetRowIdForWord(ComBox1SelectedItem, EditableWord2[0].English)).Result;
            }

            if (Data.AfterPause)
            {
                EditableWord = Data.CurrentTableWord;
                ComBox1SelectedItem = Data.CurrentNameDict;
            }

        }
        //-----------------------------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        //-----------------------------------------------------------------------------------------
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        //-----------------------------------------------------------------------------------------
        private bool _tabEditWordsIsSelected = true;
        public bool TabEditWordsIsSelected
        {
            get { return _tabEditWordsIsSelected; }
            set
            {
                if (value != _tabEditWordsIsSelected)
                {
                    _tabEditWordsIsSelected = value;
                    NotifyPropertyChanged("TabEditWordsIsSelected");
                    if (_tabEditWordsIsSelected)
                    {
                        ComBox = Task.Run(async () => await _sqlDataBaseClass.GetListTable()).Result;
                        if (!ComBox.Any(i => i == ComBox2Text))
                        {
                            ComBox2Text = "New dictionary";
                        }
                        EditableWord2.Clear();
                        if (ComBox1SelectedItem != null)
                        {
                            Task.Run(async () =>
                            {
                                TableFromDb = await _sqlDataBaseClass.GetTableFromDb(ComBox1SelectedItem);
                                LabelWordCount = TableFromDb.Count.ToString();
                            });
                        }
                    }
                }
            }
        }

        private string _labelWordCount;
        public string LabelWordCount
        {
            get { return _labelWordCount; }
            set
            {
                if (value != _labelWordCount)
                {
                    _labelWordCount = value;
                    NotifyPropertyChanged("LabelWordCount");
                }
            }
        }

        //-----------------------------------------------------------------------------------------
        private List<string> _comBox;
        //-----------------------------------------------------------------------------------------
        public List<string> ComBox
        {
            get
            {
                //_comBox = Task.Run(async () => await _sqlDataBaseClass.GetListTable()).Result;
                return _comBox;
            }
            set
            {
                if (value != _comBox)
                {
                    //_comBox = Task.Run(async () => await _sqlDataBaseClass.GetListTable()).Result;
                    _comBox = value;
                    NotifyPropertyChanged("ComBox");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private int _comBox1SelectedIndex;
        //-----------------------------------------------------------------------------------------
        public int ComBox1SelectedIndex
        {
            get { return _comBox1SelectedIndex; }
            set
            {
                if (value != _comBox1SelectedIndex)
                {
                    _comBox1SelectedIndex = value;
                    NotifyPropertyChanged("ComBox1SelectedIndex");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _comBox1SelectedItem;
        //-----------------------------------------------------------------------------------------
        public string ComBox1SelectedItem
        {
            get { return _comBox1SelectedItem; }
            set
            {
                if (value != _comBox1SelectedItem)
                {
                    _comBox1SelectedItem = value;
                    NotifyPropertyChanged("ComBox1SelectedItem");
                    ListViewEW = new List<string>();
                    Task.Run(async() => { ListViewEW = await _sqlDataBaseClass.GetAllFromTableToList(_comBox1SelectedItem); });
                    TableFromDb = new DataView();
                    Task.Run(async () => 
                    {
                        TableFromDb = await _sqlDataBaseClass.GetTableFromDb(_comBox1SelectedItem);
                        LabelWordCount = TableFromDb.Count.ToString();
                    });
                    EditableWord2.Clear();
                    BtnUpdateIsEnabled = false;
                    //DataGrid1SelectedIndex = Data.Nword - 1;
                    var x = Data.Nword;
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private List<string> _listViewEW;
        //-----------------------------------------------------------------------------------------
        public List<string> ListViewEW
        {
            get { return _listViewEW; }
            set
            {
                if (value != _listViewEW)
                {

                    _listViewEW = value;
                    NotifyPropertyChanged("ListViewEW");
                    
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private DataView _tableFromDb;
        //-----------------------------------------------------------------------------------------
        public DataView TableFromDb
        {
            get
            {
                return _tableFromDb;
            }
            set
            {
                if (value != _tableFromDb)
                {
                    _tableFromDb = value;
                    NotifyPropertyChanged("TableFromDB");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private int _rowId;
        private int _dataGrid1SelectedIndex = -1;
        public int DataGrid1SelectedIndex
        {
            get { return _dataGrid1SelectedIndex; }
            set
            {
                if(value != _dataGrid1SelectedIndex)
                {
                    _dataGrid1SelectedIndex = value;
                    NotifyPropertyChanged("DataGrid1SelectedIndex");
                    if (_dataGrid1SelectedIndex >= 0)
                    {
                        string wordEn = TableFromDb[_dataGrid1SelectedIndex].Row.ItemArray[0].ToString();
                        _rowId = Task.Run(async () => await _sqlDataBaseClass.GetRowIdForWord(ComBox1SelectedItem, wordEn)).Result;
                        _editWordModel.SelectWordFromTable(TableFromDb, EditableWord, _dataGrid1SelectedIndex); 
                        _editWordModel.SelectWordFromTable(TableFromDb, EditableWord2, _dataGrid1SelectedIndex);
                        var str = EditableWord[0];
                        ComBox2IsEnabled = true;
                        if (ComBox2Text != "New dictionary")
                        {
                            BtnMoveInIsEnabled = true;
                        }
                        BtnDeleteIsEnabled = true;
                    }
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        #region ButttonUpdate
        private bool _btnUpdateIsEnabled;
        public bool BtnUpdateIsEnabled
        {
            get { return _btnUpdateIsEnabled; }
            set
            {
                if (value != _btnUpdateIsEnabled)
                {
                    _btnUpdateIsEnabled = value;
                    NotifyPropertyChanged("BtnUpdateIsEnabled");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private ICommand _btnUpdateClick;
        public ICommand BtnUpdateClick
        {
            get
            {
                return _btnUpdateClick ?? (_btnUpdateClick = new RelayCommand(async(arg) => 
                {
                    int res = await _sqlDataBaseClass.SqlQueryUpdateWord(ComBox1SelectedItem, EditableWord2, _rowId);
                    if (res > 0)
                    {
                        BtnUpdateIsEnabled = false;
                        EditableWord2.Clear();
                        TableFromDb = await _sqlDataBaseClass.GetTableFromDb(ComBox1SelectedItem);
                        MessageBox.Show(string.Format("Словарь {0} обновлен", ComBox1SelectedItem));
                    }
                }));
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        #region DataGrid2
        private int _dataGrid2SelectedIndex = -1;
        public int DataGrid2SelectedIndex
        {
            get { return _dataGrid2SelectedIndex; }
            set
            {
                if (value != _dataGrid2SelectedIndex)
                {
                    _dataGrid2SelectedIndex = value;
                    NotifyPropertyChanged("DataGrid2SelectedIndex");
                    if (_dataGrid2SelectedIndex >= 0)
                    {
                        var str = EditableWord2[0];
                        BtnDeleteIsEnabled = true;
                        //DataGrid1SelectedIndex = Data.Nword - 1;
                    }
                }
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        #region ComboBox2
        private string _comBox2Text = "New dictionary";
        //-----------------------------------------------------------------------------------------
        public string ComBox2Text
        {
            get { return _comBox2Text; }
            set
            {
                if (value != _comBox2Text)
                {
                    _comBox2Text = value;
                    NotifyPropertyChanged("ComBox2Text");
                    if (_comBox2Text != "" && EditableWord2.Count > 0)
                    {
                        BtnMoveInIsEnabled = true;
                    }
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private bool _comBox2IsEnabled;
        public bool ComBox2IsEnabled
        {
            get { return _comBox2IsEnabled; }
            set
            {
                if (value != _comBox2IsEnabled)
                {
                    _comBox2IsEnabled = value;
                    NotifyPropertyChanged("ComBox2IsEnabled");
                }
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        #region BtnMoveIn
        private bool _btnMoveInIsEnabled;
        public bool BtnMoveInIsEnabled
        {
            get { return _btnMoveInIsEnabled; }
            set
            {
                if (value != _btnMoveInIsEnabled)
                {
                    _btnMoveInIsEnabled = value;
                    NotifyPropertyChanged("BtnMoveInIsEnabled");
                }
            }
        }
        //----------------------------------------------------------------------------------------
        private ICommand _btnMoveInClick;
        public ICommand BtnMoveInClick
        {
            get
            {
                return _btnMoveInClick ?? (_btnMoveInClick = new RelayCommand(async (arg) =>
                {
                    var a = _rowId;
                    var d = ComBox1SelectedItem;
                    await _sqlDataBaseClass.CreateTableAsync(ComBox2Text);
                    
                    ComBox = Task.Run(async () => await _sqlDataBaseClass.GetListTable()).Result;
                    if (!CheckBoxCopyIsChecked)
                    {
                        var res1 = await _sqlDataBaseClass.AddWordInTableAsync(ComBox2Text, EditableWord2);
                        var res2 =  await _sqlDataBaseClass.DeleteWordFromDict(ComBox1SelectedItem, _rowId);
                        if (res1 >= 0 && res2 >= 0)
                        {
                            MessageBox.Show("Слово перемещено в словарь "+ ComBox2Text);
                        }
                    }
                    else
                    {
                        var res =  await _sqlDataBaseClass.AddWordInTableAsync(ComBox2Text, EditableWord2);
                        
                        if (res >= 0)
                        {
                            MessageBox.Show("Слово скопировано в словарь " + ComBox2Text);
                        }
                    }
                    EditableWord2.Clear();
                    TableFromDb = await _sqlDataBaseClass.GetTableFromDb(ComBox1SelectedItem);
                    BtnMoveInIsEnabled = false;
                }));
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region CheckBoxCopy
        private bool _checkBoxCopyIsChecked = false;
        public bool CheckBoxCopyIsChecked
        {
            get { return _checkBoxCopyIsChecked; }
            set
            {
                if (value != _checkBoxCopyIsChecked)
                {
                    _checkBoxCopyIsChecked = value;
                    NotifyPropertyChanged("CheckBoxCopyIsChecked");
                }
            }
        }
        #endregion
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        #region BtnDelete
        private bool _btnDeleteIsEnabled;
        public bool BtnDeleteIsEnabled
        {
            get { return _btnDeleteIsEnabled; }
            set
            {
                if (value != _btnDeleteIsEnabled)
                {
                    _btnDeleteIsEnabled = value;
                    NotifyPropertyChanged("BtnDeleteIsEnabled");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private ICommand _btnDeleteInClick;
        public ICommand BtnDeleteInClick
        {
            get
            {
                return _btnDeleteInClick ?? (_btnDeleteInClick = new RelayCommand(async (arg) =>
                {
                    await _sqlDataBaseClass.CreateTableAsync(ComBox2Text);
                    var res = await _sqlDataBaseClass.DeleteWordFromDict(ComBox1SelectedItem, _rowId);
                    if (res >= 0)
                    {
                        MessageBox.Show("Слово удалено из словаря " + ComBox1SelectedItem);
                    }
                    EditableWord2.Clear();
                    TableFromDb = await _sqlDataBaseClass.GetTableFromDb(ComBox1SelectedItem);
                    BtnDeleteIsEnabled = false;
                }));
            }
        }
        #endregion





    }
}
