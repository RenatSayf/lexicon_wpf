﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using Lexicon.ViewModel;

namespace Lexicon.Model
{
    public class EditWordModel
    {

        //-----------------------------------------------------------------------------------------
        public EditWordModel()
        {
            
        }
        //-----------------------------------------------------------------------------------------
        public ObservableCollection<CollectionWords_Model> SelectWordFromTable(DataView tableFromDb, ObservableCollection<CollectionWords_Model> editableWord, int index)
        {
            editableWord.Clear();
            editableWord.Add(new CollectionWords_Model());
            editableWord[0].English = tableFromDb[index].Row.ItemArray[0].ToString();
            editableWord[0].Translation = tableFromDb[index].Row.ItemArray[1].ToString();
            editableWord[0].Noun = tableFromDb[index].Row.ItemArray[2].ToString();
            editableWord[0].Verb = tableFromDb[index].Row.ItemArray[3].ToString();
            editableWord[0].Adjective = tableFromDb[index].Row.ItemArray[4].ToString();
            editableWord[0].Adverb = tableFromDb[index].Row.ItemArray[5].ToString();
            editableWord[0].Pronoun = tableFromDb[index].Row.ItemArray[6].ToString();
            editableWord[0].Preposition = tableFromDb[index].Row.ItemArray[7].ToString();
            return editableWord;
        }
        //-----------------------------------------------------------------------------------------
        public bool СompareСollections(ObservableCollection<CollectionWords_Model> collection1,
            ObservableCollection<CollectionWords_Model> collection2)
        {
            if (collection1[0].English != collection2[0].English) return false;
            if (collection1[0].Translation != collection2[0].Translation) return false;
            if (collection1[0].Noun != collection2[0].Noun) return false;
            if (collection1[0].Verb != collection2[0].Verb) return false;
            if (collection1[0].Adjective != collection2[0].Adjective) return false;
            if (collection1[0].Adverb != collection2[0].Adverb) return false;
            if (collection1[0].Pronoun != collection2[0].Pronoun) return false;
            if (collection1[0].Preposition != collection2[0].Preposition) return false;
            return true;
        }


    }
}
