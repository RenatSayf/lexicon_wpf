﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Media;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Windows.Media;
using System.Windows;
using System.Collections.ObjectModel;
using Lexicon.Model;

namespace Lexicon.Classes
{
    public class SpeakingClass
    {
        private SQLiteDataBaseClass _sqliteDataBaseClass;
        private SpeechSynthesizer _synthesizer;
        private bool _play = true;
        private TextBlock _enTextBlock;
        private TextBlock _transTextBlock;
        private List<string> _voiceEn = new List<string>();
        private List<string> _voiceRu = new List<string>();
        
        public SpeakingClass()
        {
            _synthesizer = new SpeechSynthesizer();// создание объекта типа SpeechSynthesizer
            CreateListOfVoices();
        }

        public SpeakingClass(TextBlock enTextBlock, TextBlock transTextBlock)
        {
            _enTextBlock = enTextBlock;
            _transTextBlock = transTextBlock;
            _sqliteDataBaseClass = new SQLiteDataBaseClass();
            _synthesizer = new SpeechSynthesizer();// создание объекта типа SpeechSynthesizer
            CreateListOfVoices(); 
        }

        private void CreateListOfVoices()
        {
            foreach (InstalledVoice voice in _synthesizer.GetInstalledVoices())// обход коллекции установленых голосов
            {
                VoiceInfo infoVoice = voice.VoiceInfo; // получаем информацию о каждом голосе
                if (infoVoice.Culture.Name.Equals("en-US"))
                {
                    _voiceEn.Add(infoVoice.Name);
                }
                else if (infoVoice.Culture.Name.Equals("ru-RU"))
                {
                    _voiceRu.Add(infoVoice.Name); 
                }
            }
            if (_voiceEn.Count <= 0)
            {
                MessageBox.Show(@"В Вашей системе отсутствует англоязычный(US) ситезатор речи./n
                                    Голосовые функции для этого синтезатора недоступны./n
                                    Установите голосовой пакет путем обновления Windows.");
            }
            if (_voiceRu.Count <= 0)
            {
                MessageBox.Show(@"В Вашей системе отсутствует русскоязычный(RU) ситезатор речи./n
                                    Голосовые функции для этого синтезатора недоступны./n
                                    Установите голосовой пакет путем обновления Windows.");
            }
        }

        public bool PlayStop
        {
            get { return _play; }
            set { _play = value; }
        }

        public async Task<List<string>> GetWordsFromDict(string nameDict, int index)
        {
            List<string> _listWord = new List<string>();
            _listWord = await _sqliteDataBaseClass.GetWordFromTable(nameDict, index);
            return _listWord;
        }

        public int SpeechVolume
        {
            set
            {
                _synthesizer.Volume = value;
            }
        }

        private int _Ndict = 0, _Nword = 1;

        public async Task Play(
                                int switch_on,          // Выбор слов в словаре 0 - последовательный выбор
                                                        //                      1 - обратный выбор
                                int Ndict,              // № номер словаря для начала воспроизведения (0 - с начала)
                                int Nword,              // № номер слова в словаре (1 - с начала)
                                bool afterPause         // true - воспроизведение после паузы, иначе false
                              )
        {
            agan1:
            switch_on = Properties.Settings.Default.playBackOrder;
            _play = true;
            List<string> rowWords = new List<string>();
            _transTextBlock.Foreground = Brushes.Blue;
            _enTextBlock.Foreground = Brushes.Green;
           
            if (Properties.Settings.Default.settingsPlayList.Count == 0)
            {
                _enTextBlock.Text = "Set a playlist";
                _enTextBlock.Foreground = Brushes.Red;
                _transTextBlock.Text = "";
                return;
            }
            switch (switch_on)
            {
                case 0:
                    while (_play)
                    {
                        if (!_play || Properties.Settings.Default.settingsPlayList.Count == 0) break;
                        for (int i = 0; i < Properties.Settings.Default.settingsPlayList.Count; i++)
                        {
                            Data.CurrentNameDict = Properties.Settings.Default.settingsPlayList[i];
                            if (!_play || Properties.Settings.Default.settingsPlayList.Count == 0) break;
                            try
                            {
                                int j = 1;
                                do
                                {
                                    if (!await CheckPlayListOnEmpty(i)) { _play = false; break; }

                                    _transTextBlock.Text = "";
                                    if (afterPause)
                                    {
                                        rowWords = await GetWordsFromDict(Properties.Settings.Default.settingsPlayList[Data.Ndict], Data.Nword);
                                        afterPause = false;
                                        i = Data.Ndict;
                                        j = Data.Nword;
                                    }
                                    else
                                    {
                                        rowWords = await GetWordsFromDict(Properties.Settings.Default.settingsPlayList[i], j);
                                    }

                                    for (int z = 0; z < rowWords.Count; z++)
                                    {
                                        if (!_play || Properties.Settings.Default.settingsPlayList.Count == 0)
                                        {
                                            _Ndict = i;
                                            _Nword = j;
                                            break;
                                        }
                                        if (rowWords[z] == "") continue;
                                        _indexDict = i;
                                        _indexWord = j;
                                        await SpeakWord(rowWords[z], _enTextBlock, _transTextBlock);
                                    }
                                    if (Data.IsPlayListChanged) { Data.IsPlayListChanged = false; goto agan1; }
                                    if (!_play || Properties.Settings.Default.settingsPlayList.Count == 0) break;
                                    await SpeakWord(rowWords[0], _enTextBlock, _transTextBlock);
                                    j++;

                                } while (j <= await _sqliteDataBaseClass.NumberRecords(Properties.Settings.Default.settingsPlayList[i]));
                            }
                            catch(ArgumentOutOfRangeException)
                            {
                                _play = false;
                                break;
                            }
                        }
                    }
                    break;
                case 1:
                    while (_play)
                    {
                        if (!_play || Properties.Settings.Default.settingsPlayList.Count == 0) break;
                        for (int i = 0; i < Properties.Settings.Default.settingsPlayList.Count; i++)
                        {
                            Data.CurrentNameDict = Properties.Settings.Default.settingsPlayList[i];
                            if (!_play || Properties.Settings.Default.settingsPlayList.Count == 0) break;
                            try
                            {
                                int j = await _sqliteDataBaseClass.NumberRecords(Properties.Settings.Default.settingsPlayList[i]);
                                do
                                {
                                    if (!await CheckPlayListOnEmpty(i)) { _play = false; break; }

                                    _transTextBlock.Text = "";
                                    if (afterPause)
                                    {
                                        rowWords = await GetWordsFromDict(Properties.Settings.Default.settingsPlayList[Data.Ndict], Data.Nword);
                                        afterPause = false;
                                        i = Data.Ndict;
                                        j = Data.Nword;
                                    }
                                    else
                                    {
                                        rowWords = await GetWordsFromDict(Properties.Settings.Default.settingsPlayList[i], j);
                                    }

                                    for (int z = 0; z < rowWords.Count; z++)
                                    {
                                        if (!_play || Properties.Settings.Default.settingsPlayList.Count == 0)
                                        {
                                            _Ndict = i;
                                            _Nword = j;
                                            break;
                                        }
                                        if (rowWords[z] == "") continue;
                                        _indexDict = i;
                                        _indexWord = j;
                                        await SpeakWord(rowWords[z], _enTextBlock, _transTextBlock);
                                    }
                                    if (Data.IsPlayListChanged) { Data.IsPlayListChanged = false; goto agan1; }
                                    if (!_play || Properties.Settings.Default.settingsPlayList.Count == 0) break;
                                    await SpeakWord(rowWords[0], _enTextBlock, _transTextBlock);
                                    j--;

                                } while (j >= 1);
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                _play = false;
                                break;
                            }
                        }
                    }
                    break;
            }
        }

        private async Task SpeakWord(string str, TextBlock enTextBlock, TextBlock transTextBlock)
        {
            Prompt res = null;
            if (str.Any(wordByte => wordByte > 127))
            {
                _synthesizer.SelectVoice(_voiceRu[0]);
                transTextBlock.Text = str;
            }
            else
            {
                _synthesizer.SelectVoice(_voiceEn[0]);
                enTextBlock.Text = str + " - ";
            }
            res = _synthesizer.SpeakAsync(str);
            while (!res.IsCompleted)
            {
                await Task.Delay(10);
            }
            
            Data.Ndict = _indexDict;
            Data.Nword = _indexWord;
            return;
        }

        public async Task SpeakWord(string str)
        {
            Prompt res = null;
            if (str.Any(wordByte => wordByte > 127))
            {
                _synthesizer.SelectVoice(_voiceRu[0]);
            }
            else
            {
                _synthesizer.SelectVoice(_voiceEn[0]);
            }
            res = _synthesizer.SpeakAsync(str);
            while (!res.IsCompleted)
            {
                await Task.Delay(1);
            }
            return;
        }

        private async Task<bool> CheckPlayListOnEmpty(int index)
        {
            string name = Properties.Settings.Default.settingsPlayList[index];
            int numberRecords = await _sqliteDataBaseClass.NumberRecords(name);
            if (numberRecords == 0)
            {
                _play = false;
                _transTextBlock.Text = "";
                _enTextBlock.Foreground = Brushes.Red;
                _enTextBlock.Text = string.Format(@"Словарь <<{0}>> пуст. Добавьте новые слова", name);
                return false;
            }
            return true;
        }

        private int _indexWord = 1;
        private int _indexDict = 0;

        public void Stop()
        {
            _play = false;
            _synthesizer.SpeakAsyncCancelAll();
            Data.IsPlay = false;
            Data.Ndict = 0;
            Data.Nword = 1;
            Data.AfterPause = false;
        }

        public void Pause()
        {
            _play = false;
            _synthesizer.SpeakAsyncCancelAll();
            Data.IsPlay = false;
            Data.Ndict = _indexDict;
            Data.Nword = _indexWord;
            Data.AfterPause = true;
        }

        public bool IsPlay
        {
            get { return _play; }
        }

        public async void GetNextWord()
        {
            Pause();
            int nextIndexWord = 1;
            int nextIndexDict = 0;
            int numberRecords = await _sqliteDataBaseClass.NumberRecords(Properties.Settings.Default.settingsPlayList[Data.Ndict]);
            if (numberRecords == 0) return;
            if (Data.Nword <= numberRecords - 1 && Data.Ndict <= Properties.Settings.Default.settingsPlayList.Count - 1)
            {
                nextIndexWord = _indexWord + 1;
                nextIndexDict = _indexDict;
            }
            else if (Data.Nword >= numberRecords && Data.Ndict < Properties.Settings.Default.settingsPlayList.Count - 1)
            {
                nextIndexWord = 1;
                nextIndexDict = _indexDict + 1;
            }
            else if (Data.Nword >= numberRecords && Data.Ndict >= Properties.Settings.Default.settingsPlayList.Count - 1)
            {
                nextIndexWord = 1;
                nextIndexDict = 0;
            }
            else if (Data.Nword == numberRecords && Data.Ndict == 0 && Properties.Settings.Default.settingsPlayList.Count == 1)
            {
                nextIndexWord = 1;
                nextIndexDict = 0;
            }
            _indexDict = nextIndexDict;
            _indexWord = nextIndexWord;
            _Ndict = _indexDict;
            _Nword = _indexWord;
            Data.Ndict = _indexDict;
            Data.Nword = _indexWord;
            Data.CurrentNameDict = Properties.Settings.Default.settingsPlayList[nextIndexDict];
            var rowWords = await GetWordsFromDict(Properties.Settings.Default.settingsPlayList[Data.Ndict], Data.Nword);
            if (rowWords.Count == 0) return;
            _enTextBlock.Text = rowWords[0] + " - ";
            for (int i = 1; i < rowWords.Count; i++)
            {
                if (rowWords[i] != "")
                {
                    _transTextBlock.Text = rowWords[i]; break;
                }
            }
        }

        public async void GetPeviousWord()
        {
            Pause();
            int nextIndexWord = await _sqliteDataBaseClass.NumberRecords(Properties.Settings.Default.settingsPlayList[Properties.Settings.Default.settingsPlayList.Count - 1]);
            int nextIndexDict = Properties.Settings.Default.settingsPlayList.Count - 1;
            try
            {
                if (Data.Nword > 1 && Data.Ndict >= 0)
                {
                    nextIndexWord = _indexWord - 1;
                    nextIndexDict = _indexDict;
                }
                else if (Data.Nword < 1 && Data.Ndict >= Properties.Settings.Default.settingsPlayList.Count - 1)
                {
                    nextIndexWord = await _sqliteDataBaseClass.NumberRecords(Properties.Settings.Default.settingsPlayList[Data.Ndict/*_indexDict*/ + 1]);
                    nextIndexDict = _indexDict + 1;
                }
                else if (Data.Nword <= 1 && Data.Ndict >= Properties.Settings.Default.settingsPlayList.Count - 1 && Properties.Settings.Default.settingsPlayList.Count > 1)
                {
                    nextIndexWord = await _sqliteDataBaseClass.NumberRecords(Properties.Settings.Default.settingsPlayList[Data.Ndict - 1]);
                    nextIndexDict = _indexDict - 1;
                }
                else if (Data.Nword == 1 && Data.Ndict == 0 && Properties.Settings.Default.settingsPlayList.Count == 1)
                {
                    nextIndexWord = await _sqliteDataBaseClass.NumberRecords(Properties.Settings.Default.settingsPlayList[Data.Ndict]);
                    nextIndexDict = 0;
                }
            }
            catch (Exception)
            {
                return;
            }
            _indexDict = nextIndexDict;
            _indexWord = nextIndexWord;
            _Ndict = _indexDict;
            _Nword = _indexWord;
            Data.Ndict = _indexDict;
            Data.Nword = _indexWord;
            Data.CurrentNameDict = Properties.Settings.Default.settingsPlayList[nextIndexDict];
            var rowWords = await GetWordsFromDict(Properties.Settings.Default.settingsPlayList[Data.Ndict], Data.Nword);
            if (rowWords.Count == 0) return;
            _enTextBlock.Text = rowWords[0] + " - ";
            for (int i = 1; i < rowWords.Count; i++)
            {
                if (rowWords[i] != "")
                {
                    _transTextBlock.Text = rowWords[i]; break;
                }
            }
        }

        public async Task<List<string>> GetCurrentWord()
        {
            List<string> listword = new List<string> { "", "", "", "", "", "", "", "", "" };
            if (Properties.Settings.Default.settingsPlayList.Count > 0)
            {
                listword = await GetWordsFromDict(Properties.Settings.Default.settingsPlayList[_indexDict].ToString(), _indexWord);
                listword.Add(Properties.Settings.Default.settingsPlayList[_indexDict].ToString());
            }
            
            return listword;
        }

        public async Task<ObservableCollection<CollectionWords_Model>> GetCurrentWord2()
        {
            ObservableCollection<CollectionWords_Model> word = new ObservableCollection<CollectionWords_Model>();
            word.Add(new CollectionWords_Model());
            //List<string> listword = new List<string>();
            List<string> listword = new List<string> { "", "", "", "", "", "", "", "", "" };
            if (Properties.Settings.Default.settingsPlayList.Count > 0)
            {
                listword = await GetWordsFromDict(Properties.Settings.Default.settingsPlayList[_indexDict].ToString(), _indexWord);
                word[0].English = listword[0];
                word[0].Translation = listword[1];
                word[0].Noun = listword[2];
                word[0].Verb = listword[3];
                word[0].Adjective = listword[4];
                word[0].Adverb = listword[5];
                word[0].Pronoun = listword[6];
                word[0].Preposition = listword[7];
            }
            return word;
        }







    }
}
