﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Lexicon.Classes;
using Lexicon.Model;
using System.Windows.Controls.Primitives;

namespace Lexicon.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public AddWord winAddWord;

        //-----------------------------------------------------------------------------------------
        public MainWindowViewModel()
        {
                        
        }
        //-----------------------------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        //-----------------------------------------------------------------------------------------
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        //-----------------------------------------------------------------------------------------
        private bool _menuIsOpen;
        public bool MenuIsOpen
        {
            get { return _menuIsOpen; }
            set
            {
                if (value != _menuIsOpen)
                {
                    _menuIsOpen = value;
                    NotifyPropertyChanged("MenuIsOpen");
                }
            }
        }

        private bool _isTopMost = Properties.Settings.Default.isTopMost;
        public bool IsTopMost
        {
            get { return _isTopMost; }
            set
            {
                if (value != _isTopMost)
                {
                    _isTopMost = value;
                    NotifyPropertyChanged("IsTopMost");
                }
            }
        }

        //-----------------------------------------------------------------------------------------
        private Visibility _mapMarker1Visible = Visibility.Collapsed;
        public Visibility MapMarker1Visible
        {
            get
            {
                return IsTopMost ? Visibility.Visible : Visibility.Collapsed;
            }
            set
            {
                _mapMarker1Visible = value;
                NotifyPropertyChanged("MapMarker1Visible");
            }
        }
        //-----------------------------------------------------------------------------------------
        private Visibility _mapMarker2Visible = Visibility.Visible;
        public Visibility MapMarker2Visible
        {
            get { return IsTopMost ? Visibility.Collapsed : Visibility.Visible; }
            set
            {
                _mapMarker2Visible = value;
                NotifyPropertyChanged("MapMarker2Visible");
            }
        }

        private ICommand _btnMenuCommand;
        public ICommand BtnMenuCommand
        {
            get
            {
                return _btnMenuCommand ?? (_btnMenuCommand = new RelayCommand((arg) =>
                {
                    MenuIsOpen = true;   
                }));
            }
        }
        //-----------------------------------------------------------------------------------------
        private ICommand _btnMapMarkerClick;
        public ICommand BtnMapMarkerClick
        {
            get
            {
                return _btnMapMarkerClick ?? (_btnMapMarkerClick = new RelayCommand((arg) => 
                {
                    if (IsTopMost)
                    {
                        IsTopMost = false;
                        MapMarker1Visible = Visibility.Visible;
                        MapMarker2Visible = Visibility.Collapsed;
                    }
                    else
                    {
                        IsTopMost = true;
                        MapMarker1Visible = Visibility.Collapsed;
                        MapMarker2Visible = Visibility.Visible;
                    }
                    Properties.Settings.Default.isTopMost = IsTopMost;
                }));
            }
        }
        
        private ICommand _btnPauseClick;
        public ICommand BtnPauseClick
        {
            get
            {
                return _btnPauseClick ?? (_btnPauseClick = new RelayCommand((arg) =>
                {
                    BtnPauseVisibility = Visibility.Collapsed;
                    BtnPlayVisibility = Visibility.Visible;
                }));
            }
        }
        //-----------------------------------------------------------------------------------------
        private ICommand _btnPlayClick;
        public ICommand BtnPlaylick
        {
            get
            {
                return _btnPlayClick ?? (_btnPlayClick = new RelayCommand((arg) =>
                {
                    BtnPlayVisibility = Visibility.Collapsed;
                    BtnPauseVisibility = Visibility.Visible;
                    BtnBackVisibility = Visibility.Visible;
                    BtnForvardVisibility = Visibility.Visible;
                    BtnStopVisibility = Visibility.Visible;
                 }));
            }
        }
        //-----------------------------------------------------------------------------------------
        private ICommand _btnStopClick;
        public ICommand BtnStopClick
        {
            get
            {
                return _btnStopClick ?? (_btnStopClick = new RelayCommand((arg) =>
                {
                    BtnPlayVisibility = Visibility.Visible;
                    BtnPauseVisibility = Visibility.Collapsed;
                    BtnBackVisibility = Visibility.Collapsed;
                    BtnForvardVisibility = Visibility.Collapsed;
                    BtnStopVisibility = Visibility.Collapsed;
                }));
            }
        }
        //-----------------------------------------------------------------------------------------
        private ICommand _btnBackClick;
        public ICommand BtnBackClick
        {
            get
            {
                return _btnBackClick ?? (_btnBackClick = new RelayCommand((arg) =>
                {
                    BtnPauseVisibility = Visibility.Collapsed;
                    BtnPlayVisibility = Visibility.Visible;
                }));
            }
        }
        //-----------------------------------------------------------------------------------------
        private ICommand _btnForvardClick;
        public ICommand BtnForvardClick
        {
            get
            {
                return _btnForvardClick ?? (_btnForvardClick = new RelayCommand((arg) =>
                {
                    BtnPauseVisibility = Visibility.Collapsed;
                    BtnPlayVisibility = Visibility.Visible;
                }));
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _textBlockEn = "Zubrilka Words";
        public string TextBlockEn
        {
            get { return _textBlockEn; }
            set
            {
                if (value != _textBlockEn)
                {
                    _textBlockEn = value;
                    NotifyPropertyChanged("TextBlockEn");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _textBlockRu;
        public string TextBlockRu
        {
            get { return _textBlockRu; }
            set
            {
                if (value != _textBlockRu)
                {
                    _textBlockRu = value;
                    NotifyPropertyChanged("TextBlockRu");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private Visibility _btnBackVisibility = Visibility.Collapsed;
        public Visibility BtnBackVisibility
        {
            get { return _btnBackVisibility; }
            set
            {
                if (value != _btnBackVisibility)
                {
                    _btnBackVisibility = value;
                    NotifyPropertyChanged("BtnBackVisibility");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private Visibility _btnPlayVisibility;
        public Visibility BtnPlayVisibility
        {
            get { return _btnPlayVisibility; }
            set
            {
                if (value != _btnPlayVisibility)
                {
                    _btnPlayVisibility = value;
                    NotifyPropertyChanged("BtnPlayVisibility");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private Visibility _btnPauseVisibility = Visibility.Collapsed;
        public Visibility BtnPauseVisibility
        {
            get { return _btnPauseVisibility; }
            set
            {
                if (value != _btnPauseVisibility)
                {
                    _btnPauseVisibility = value;
                    NotifyPropertyChanged("BtnPauseVisibility");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private Visibility _btnStopVisibility = Visibility.Collapsed;
        public Visibility BtnStopVisibility
        {
            get { return _btnStopVisibility; }
            set
            {
                if (value != _btnStopVisibility)
                {
                    _btnStopVisibility = value;
                    NotifyPropertyChanged("BtnStopVisibility");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private Visibility _btnForvardVisibility = Visibility.Collapsed;
        public Visibility BtnForvardVisibility
        {
            get { return _btnForvardVisibility; }
            set
            {
                if (value != _btnForvardVisibility)
                {
                    _btnForvardVisibility = value;
                    NotifyPropertyChanged("BtnForvardVisibility");
                }
            }
        }

        





    }
}
