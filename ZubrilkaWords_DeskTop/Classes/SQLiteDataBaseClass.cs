﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using Lexicon.Model;
using System.Diagnostics;

namespace Lexicon.Classes
{
    public class SQLiteDataBaseClass
    {
        public static string DataBaseName = Directory.GetCurrentDirectory() + "\\WordsDB.db";
        private List<string> _list;
        private string _cmdGetTableName = @"SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;";
        private string _cmdCreateTableIfNotExists = @"CREATE TABLE IF NOT EXISTS {0}
                                                    (
	                                                    {1} VARCHAR NOT NULL,
	                                                    {2} VARCHAR NULL,
                                                        {3} VARCHAR NULL,
	                                                    {4} VARCHAR NULL,
	                                                    {5} VARCHAR NULL,
	                                                    {6} VARCHAR NULL,
	                                                    {7} VARCHAR(10) NULL,
	                                                    {8} VARCHAR(10) NULL
                                                    )";
        private string _cmdRemoveTable = @"Drop Table If Exists '{0}'";
        private string _cmdGetAllFromTable= @"SELECT * FROM '{0}';";
        private string _cmdGetNextFiveWords = @"SELECT * FROM '{0}' WHERE RowID BETWEEN {1} AND {2};";
        
        //-----------------------------------------------------------------------------------------
        public SQLiteDataBaseClass()
        {
            _list = new List<string>();
            //_list.Add("ID");
            _list.Add("English");
            _list.Add("Translation"); 
            _list.Add("Noun");
            _list.Add("Verb");
            _list.Add("Adjective");
            _list.Add("Adverb");
            _list.Add("Pronoun");
            _list.Add("Preposition");
            //GetListTable();
            
        }
        //-----------------------------------------------------------------------------------------
        public SQLiteConnection SetConnectToDataBase()
        {
            return new SQLiteConnection(string.Format("Data Source={0};", DataBaseName));
        }
        //-----------------------------------------------------------------------------------------
        public SQLiteConnection SetConnectToOtherDB(string path)
        {
            return new SQLiteConnection(string.Format("Data Source={0};", path));
        }
        //-----------------------------------------------------------------------------------------
        public int CreateTable(string name)
        {
            int res = -1;
            name = Trim(name);
            name = name.Replace(' ', '_');
            string cmd = string.Format(_cmdCreateTableIfNotExists, name, _list[0], _list[1], _list[2], _list[3], _list[4], _list[5], _list[6], _list[7]);
            SQLiteConnection connection = SetConnectToDataBase();
            SQLiteCommand sqlitecommand = new SQLiteCommand(cmd, connection);
            try
            {
                connection.Open();
                res = sqlitecommand.ExecuteNonQuery();
                connection.Close(); //закрываем базу
            }
            catch (Exception err) { Debug.WriteLine(err.Message); }
            finally
            {
                //if (connection != null) connection.Dispose();
                if (sqlitecommand != null) sqlitecommand.Dispose();
            }
            return res;
        }
        //-----------------------------------------------------------------------------------------
        public async Task<int> CreateTableAsync(string name)
        {
            name = Trim(name);
            name = name.Replace(' ', '_');
            name = name.Trim();
            int res = -1;
            string cmd = string.Format(_cmdCreateTableIfNotExists, name, _list[0], _list[1], _list[2], _list[3], _list[4], _list[5], _list[6], _list[7]);
            SQLiteConnection connection = SetConnectToDataBase();
            SQLiteCommand sqlitecommand = new SQLiteCommand(cmd, connection);
            try
            {
                connection.Open();
                res = await sqlitecommand.ExecuteNonQueryAsync();
                connection.Close(); //закрываем базу
            }
            catch (Exception err) { Debug.WriteLine(err.Message); }
            finally
            {
                //if (connection != null) connection.Dispose();
                if (sqlitecommand != null) sqlitecommand.Dispose();
            }
            return res;
        }
        //-----------------------------------------------------------------------------------------
        public async Task<List<string>> GetListTable()
        {
            List<string> list = new List<string>();
            SQLiteConnection connection = SetConnectToDataBase();
            SQLiteCommand sqlitecommand = new SQLiteCommand(_cmdGetTableName, connection);
            try
            {
                connection.Open();
                var reader = await sqlitecommand.ExecuteReaderAsync();
                string name = "";
                while (reader.Read())
                {
                    name = (string)reader["name"];
                    name = name.Replace('_', ' ');
                    list.Add(name);
                }
                connection.Close();
            }
            catch (Exception err) { Debug.WriteLine(err.Message); }
            finally
            {
                //if (connection != null) connection.Dispose();
                if (sqlitecommand != null) sqlitecommand.Dispose();
            }
            return list;
        }
        //-----------------------------------------------------------------------------------------
        public async Task<List<string>> GetListTable(string path)
        {
            List<string> list = new List<string>();
            SQLiteConnection connection = SetConnectToOtherDB(path);
            SQLiteCommand sqlitecommand = new SQLiteCommand(_cmdGetTableName, connection);
            try
            {
                connection.Open();
                var reader = await sqlitecommand.ExecuteReaderAsync();
                string name = "";
                while (reader.Read())
                {
                    name = (string)reader["name"];
                    name = name.Replace('_', ' ');
                    list.Add(name);
                }
                connection.Close();
            }
            catch (Exception err) { Debug.WriteLine(err.Message); }
            finally
            {
                //if (connection != null) connection.Dispose();
                if (sqlitecommand != null) sqlitecommand.Dispose();
            }
            return list;
        }
        //-----------------------------------------------------------------------------------------
        public int RemoveTable(string name)
        {
            int res = -1;
            name = name.Replace(' ', '_');
            name = name.Trim();
            string cmd = string.Format(_cmdRemoveTable, name);
            SQLiteConnection connection = SetConnectToDataBase();
            SQLiteCommand sqlitecommand = new SQLiteCommand(cmd, connection);
            try
            {
                connection.Open();
                res = sqlitecommand.ExecuteNonQuery();

                connection.Close(); //закрываем базу
            }
            catch (Exception err) { Debug.WriteLine(err.Message); }
            finally
            {
                //if (connection != null) connection.Dispose();
                if (sqlitecommand != null) sqlitecommand.Dispose();
            }
            return res;
        }
        //-----------------------------------------------------------------------------------------
        public async Task<List<string>> GetAllFromTableToList(string tableName)
        {
            tableName = tableName.Replace(' ', '_');
            List<string> list = new List<string>();
            string cmd = string.Format(_cmdGetAllFromTable, tableName);
            SQLiteConnection connection = SetConnectToDataBase();
            SQLiteCommand sqlitecommand = new SQLiteCommand(cmd, connection);
            string row = null;
            try
            {
                connection.Open();
                var reader = await sqlitecommand.ExecuteReaderAsync();
                foreach (DbDataRecord record in reader)
                {
                    row = record[0].ToString() + " - ";
                    for (int i = 1; i < record.FieldCount; i++)
                    {
                        if (record[i].ToString() == "") continue;
                        row += record[i].ToString() + ", ";
                    }
                    list.Add(row);
                }
                connection.Close();
            }
            catch (Exception err) { Debug.WriteLine(err.Message); }
            finally
            {
                //if (connection != null) connection.Dispose();
                if (sqlitecommand != null) sqlitecommand.Dispose();
            }
            return list;
        }
        //-----------------------------------------------------------------------------------------
        public async Task<DataView> GetTableFromDb(string tableName)
        {
            tableName = tableName.Replace(' ', '_');
            DataTable table = new DataTable();
            string cmd = string.Format(_cmdGetAllFromTable, tableName);
            SQLiteConnection connection = SetConnectToDataBase();
            SQLiteCommand sqlitecommand = new SQLiteCommand(cmd, connection);
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlitecommand);
            try
            {
                connection.Open();
                await sqlitecommand.ExecuteNonQueryAsync();
                //SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlitecommand);
                adapter.Fill(table);
            }
            catch (Exception err) { Debug.WriteLine(err.Message); }
            finally
            {
                //if (connection != null) connection.Dispose();
                if (sqlitecommand != null) sqlitecommand.Dispose();
                if (table != null) table.Dispose();
                if (adapter != null) adapter.Dispose();
            }

            return table.DefaultView;
        }
        //-----------------------------------------------------------------------------------------
        public async Task<DataView> GetTableFromDb(string path, string tableName)
        {
            tableName = tableName.Replace(' ', '_');
            DataTable table = new DataTable();
            string cmd = string.Format(_cmdGetAllFromTable, tableName);
            SQLiteConnection connection = SetConnectToOtherDB(path);
            SQLiteCommand sqlitecommand = new SQLiteCommand(cmd, connection);
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlitecommand);
            try
            {
                connection.Open();
                await sqlitecommand.ExecuteNonQueryAsync();
                //SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlitecommand);
                adapter.Fill(table);
            }
            catch(Exception err) { Debug.WriteLine(err.Message); }
            finally
            {
                //if (connection != null) connection.Dispose();
                if (sqlitecommand != null) sqlitecommand.Dispose();
                if (table != null) table.Dispose();
                if (adapter != null) adapter.Dispose();
            }
            return table.DefaultView;
        }
        //-----------------------------------------------------------------------------------------
        public int AddWordInTable(string tableName, ObservableCollection<CollectionWords_Model> newWord)
        {
            int res = 0;
            
            tableName = tableName.Replace(' ', '_');
            newWord[0].English = Trim(newWord[0].English);
            if (newWord[0].English.Length == 0 || tableName.Length == 0)
            {
                return res;
            }
            newWord[0].Translation = Trim(newWord[0].Translation);
            newWord[0].Noun = Trim(newWord[0].Noun);
            newWord[0].Verb = Trim(newWord[0].Verb);
            newWord[0].Adjective = Trim(newWord[0].Adjective);
            newWord[0].Adverb = Trim(newWord[0].Adverb);
            newWord[0].Pronoun = Trim(newWord[0].Pronoun);
            newWord[0].Preposition = Trim(newWord[0].Preposition);
            string cmd = string.Format(@"INSERT INTO '{8}'
                                                                (   'English',
                                                                    'Translation',
                                                                    'Noun',
                                                                    'Verb',
                                                                    'Adjective',
                                                                    'Adverb',
                                                                    'Pronoun',
                                                                    'Preposition'
                                                                 ) 
                    VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}');", newWord[0].English, newWord[0].Translation, newWord[0].Noun, newWord[0].Verb, newWord[0].Adjective, newWord[0].Adverb, newWord[0].Pronoun, newWord[0].Preposition, tableName);

            SQLiteConnection connection = SetConnectToDataBase();
            SQLiteCommand sqlitecommand = new SQLiteCommand(cmd, connection);
            try
            {
                connection.Open();
                res = sqlitecommand.ExecuteNonQuery();
                connection.Close(); //закрываем базу
            }
            catch (Exception err) { Debug.WriteLine(err.Message); }
            finally
            {
                //if (connection != null) connection.Dispose();
                if (sqlitecommand != null) sqlitecommand.Dispose();
            }
            return res;
        }
        //-----------------------------------------------------------------------------------------
        public async Task<int> AddWordInTableAsync(string tableName, ObservableCollection<CollectionWords_Model> newWord)
        {
            int res = -1;
            tableName = tableName.Replace(' ', '_');
            newWord[0].English = Trim(newWord[0].English);
            newWord[0].Translation = Trim(newWord[0].Translation);
            newWord[0].Noun = Trim(newWord[0].Noun);
            newWord[0].Verb = Trim(newWord[0].Verb);
            newWord[0].Adjective = Trim(newWord[0].Adjective);
            newWord[0].Adverb = Trim(newWord[0].Adverb);
            newWord[0].Pronoun = Trim(newWord[0].Pronoun);
            newWord[0].Preposition = Trim(newWord[0].Preposition);
            string cmd = string.Format(@"INSERT INTO '{8}'
                                                                (   'English',
                                                                    'Translation',
                                                                    'Noun',
                                                                    'Verb',
                                                                    'Adjective',
                                                                    'Adverb',
                                                                    'Pronoun',
                                                                    'Preposition'
                                                                 ) 
                    VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}');", newWord[0].English, newWord[0].Translation, newWord[0].Noun, newWord[0].Verb, newWord[0].Adjective, newWord[0].Adverb, newWord[0].Pronoun, newWord[0].Preposition, tableName);

            SQLiteConnection connection = SetConnectToDataBase();
            SQLiteCommand sqlitecommand = new SQLiteCommand(cmd, connection);
            try
            {
                connection.Open();
                res = await sqlitecommand.ExecuteNonQueryAsync();
                connection.Close(); //закрываем базу
            }
            catch(Exception err) { Debug.WriteLine(err.Message); }
            finally
            {
                //if (connection != null) connection.Dispose();
                if (sqlitecommand != null) sqlitecommand.Dispose();
            }
            return res;
        }
        //-----------------------------------------------------------------------------------------
        public async Task<int> AddWordInTableAsync(string tableName, CollectionWords_Model newWord)
        {
            int res = -1;
            tableName = tableName.Replace(' ', '_');
            newWord.English = Trim(newWord.English);
            newWord.Translation = Trim(newWord.Translation);
            newWord.Noun = Trim(newWord.Noun);
            newWord.Verb = Trim(newWord.Verb);
            newWord.Adjective = Trim(newWord.Adjective);
            newWord.Adverb = Trim(newWord.Adverb);
            newWord.Pronoun = Trim(newWord.Pronoun);
            newWord.Preposition = Trim(newWord.Preposition);
            string cmd = string.Format(@"INSERT INTO '{8}'
                                                                (   'English',
                                                                    'Translation',
                                                                    'Noun',
                                                                    'Verb',
                                                                    'Adjective',
                                                                    'Adverb',
                                                                    'Pronoun',
                                                                    'Preposition'
                                                                 ) 
                    VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}');", newWord.English, newWord.Translation, newWord.Noun, newWord.Verb, newWord.Adjective, newWord.Adverb, newWord.Pronoun, newWord.Preposition, tableName);

            SQLiteConnection connection = SetConnectToDataBase();
            SQLiteCommand sqlitecommand = new SQLiteCommand(cmd, connection);
            try
            {
                connection.Open();
                res = await sqlitecommand.ExecuteNonQueryAsync();
                connection.Close(); //закрываем базу
            }
            catch (Exception err) { Debug.WriteLine(err.Message); }
            finally
            {
                //if (connection != null) connection.Dispose();
                if (sqlitecommand != null) sqlitecommand.Dispose();
            }
            return res;
        }
        //-----------------------------------------------------------------------------------------
        private string Trim(string text)
        {
            bool flag = true;
            char a;
            char b;
            if (text != null && text != "")
            {
                while (flag)
                {
                    try
                    {
                        a = text.First();
                        b = text.Last();
                    }
                    catch (Exception)
                    {
                        text = "";
                        break;
                    }
                    if (!char.IsLetter(a) && !char.IsNumber(a))
                    {
                        text = text.Trim(a); continue;
                    }
                    if (!char.IsLetter(b) && !char.IsNumber(b))
                    {
                        text = text.Trim(b); continue;
                    }
                    flag = false;
                }
                text = text.Replace("'", "''");
            }
            
            return text;
        }
        //-----------------------------------------------------------------------------------------
        public async Task<List<string>> GetWordFromTable(string tableName, int rowId)
        {
            tableName = tableName.Replace(' ', '_');
            List<string> list = new List<string>();
            string cmd = string.Format(@"SELECT * FROM '{0}' WHERE RowID = {1}", tableName, rowId);
            SQLiteConnection connection = SetConnectToDataBase();
            SQLiteCommand sqlitecommand = new SQLiteCommand(cmd, connection);
            try
            {
                tableName = tableName.Replace(' ', '_');
                connection.Open();
                var reader = await sqlitecommand.ExecuteReaderAsync();
                foreach (DbDataRecord record in reader)
                {
                    for (int i = 0; i < record.FieldCount; i++)
                    {
                        list.Add(record[i].ToString());
                    }
                }
                connection.Close();
            }
            catch (Exception err)
            {
                int x = await NumberRecords(tableName);
                MessageBox.Show(string.Format("Исключение в SQLiteDataBaseClass.GetWordFromTable():\nКоличество слов в {0} = {1}\n{2}", tableName, x, err));
            }
            finally
            {
                //if (connection != null) connection.Dispose();
                if (sqlitecommand != null) sqlitecommand.Dispose();
            }
            return list;
        }
        //-----------------------------------------------------------------------------------------
        public async Task<DataView> GetNextFiveWosdsTable(string tableName, int startId, int endId)
        {
            tableName = tableName.Replace(' ', '_');
            DataTable table = new DataTable();
            string cmd = string.Format(_cmdGetNextFiveWords, tableName, startId, endId);
            SQLiteConnection connection = SetConnectToDataBase();
            SQLiteCommand sqlitecommand = new SQLiteCommand(cmd, connection);
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlitecommand);
            try
            {
                connection.Open();
                await sqlitecommand.ExecuteNonQueryAsync();
                //SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlitecommand);
                adapter.Fill(table);
                connection.Close();
            }
            catch(Exception err) { Debug.WriteLine(err.Message); }
            finally
            {
                if (table != null)((IDisposable)table).Dispose();
                if (connection != null) (connection).Dispose();
                if (sqlitecommand != null) (sqlitecommand).Dispose();
                if (adapter != null) (adapter).Dispose();
            }

            return table.DefaultView;
        }
        //-----------------------------------------------------------------------------------------
        public async Task<int> GetRowIdForWord(string tableName, string word)
        {
            int res = -1;
            tableName = tableName.Replace(' ', '_');
            word = Trim(word);
            string cmd = string.Format(@"SELECT RowID FROM '{0}' Where English ='{1}'", tableName, word);
            SQLiteConnection connection = SetConnectToDataBase();
            SQLiteCommand sqlitecommand = new SQLiteCommand(cmd, connection);
            try
            {
                connection.Open();
                var reader = await sqlitecommand.ExecuteScalarAsync();
                string readerStr = reader.ToString();
                res = int.Parse(readerStr);
            }
            catch (Exception err) { Debug.WriteLine(err.Message); }
            finally
            {
                //if (connection != null) (connection).Dispose();
                if (sqlitecommand != null) (sqlitecommand).Dispose();
            }
            return res;
        }
        //-----------------------------------------------------------------------------------------
        public async Task<int> NumberRecords(string tableName)
        {
            int amount = 0;
            tableName = tableName.Replace(' ', '_');
            string cmd = string.Format(@"SELECT count(RowID) FROM {0}", tableName);
            SQLiteConnection connection = SetConnectToDataBase();
            SQLiteCommand sqlitecommand = new SQLiteCommand(cmd, connection);
            try
            {
                connection.Open();
                var reader = await sqlitecommand.ExecuteScalarAsync();
                string amountStr = reader.ToString();
                amount = int.Parse(amountStr);
            }
            catch (Exception err) { Debug.WriteLine(err.Message); }
            finally
            {
                //if (connection != null) (connection).Dispose();
                if (sqlitecommand != null) (sqlitecommand).Dispose();
            }
            return amount;
        }
        //-----------------------------------------------------------------------------------------
        public async Task<int> SqlQueryUpdateWord(string tableName, ObservableCollection<CollectionWords_Model> editableWord, int rowId)
        {
            int res = -1;
            tableName = tableName.Replace(' ', '_');
            editableWord[0].English = Trim(editableWord[0].English);
            editableWord[0].Translation = Trim(editableWord[0].Translation);
            editableWord[0].Noun = Trim(editableWord[0].Noun);
            editableWord[0].Verb = Trim(editableWord[0].Verb);
            editableWord[0].Adjective = Trim(editableWord[0].Adjective);
            editableWord[0].Adverb = Trim(editableWord[0].Adverb);
            editableWord[0].Pronoun = Trim(editableWord[0].Pronoun);
            editableWord[0].Preposition = Trim(editableWord[0].Preposition);
            string cmd = string.Format(@"UPDATE '{0}' SET 
                                                            English = '{1}',
                                                            Translation = '{2}',
                                                            Noun = '{3}',
                                                            Verb = '{4}',
                                                            Adjective = '{5}',
                                                            Adverb = '{6}',
                                                            Pronoun = '{7}',
                                                            Preposition = '{8}'
                                                            WHERE RowID = {9};",
                                                                tableName,
                                                                editableWord[0].English,
                                                                editableWord[0].Translation,
                                                                editableWord[0].Noun,
                                                                editableWord[0].Verb,
                                                                editableWord[0].Adjective,
                                                                editableWord[0].Adverb,
                                                                editableWord[0].Pronoun,
                                                                editableWord[0].Preposition,
                                                                rowId
                                                                );
            SQLiteConnection connection = SetConnectToDataBase();
            SQLiteCommand sqlitecommand = new SQLiteCommand(cmd, connection);
            try
            {
                connection.Open();
                res = await sqlitecommand.ExecuteNonQueryAsync();
                connection.Close(); //закрываем базу
            }
            catch(Exception err) { Debug.WriteLine(err.Message); }
            finally
            {
                //if (connection != null) (connection).Dispose();
                if (sqlitecommand != null) (sqlitecommand).Dispose();
            }
            return res;
        }
        //-----------------------------------------------------------------------------------------
        public async Task<int> SqlQueryRenameTable(string oldName, string newName)
        {
            int res = -1;
            oldName = Trim(oldName);
            newName = Trim(newName);
            oldName = oldName.Replace(' ', '_');
            newName = newName.Replace(' ', '_');
            string cmd = string.Format(@"ALTER TABLE '{0}' RENAME TO '{1}';", oldName, newName);
            SQLiteConnection connection = SetConnectToDataBase();
            SQLiteCommand sqlitecommand = new SQLiteCommand(cmd, connection);
            try
            {
                connection.Open();
                res = await sqlitecommand.ExecuteNonQueryAsync();

                connection.Close(); //закрываем базу
            }
            catch (Exception err) { Debug.WriteLine(err.Message); }
            finally
            {
                //if (connection != null) (connection).Dispose();
                if (sqlitecommand != null) (sqlitecommand).Dispose();
            }
            return res;
        }
        //-----------------------------------------------------------------------------------------
        public async Task<int> DeleteWordFromDict(string tableName, int rowId)
        {
            int res = -1;
            tableName = tableName.Replace(' ', '_');
            string cmd = string.Format(@"DELETE FROM '{0}' WHERE RowID={1}", tableName, rowId);
            SQLiteConnection connection = SetConnectToDataBase();
            SQLiteCommand sqlitecommand = new SQLiteCommand(cmd, connection);
            try
            {
                connection.Open();
                res = await sqlitecommand.ExecuteNonQueryAsync();

                cmd = string.Format(@"VACUUM;");
                sqlitecommand = new SQLiteCommand(cmd, connection);
                await sqlitecommand.ExecuteNonQueryAsync();
                connection.Close(); //закрываем базу
            }
            catch (Exception err) { Debug.WriteLine(err.Message); }
            finally
            {
                //if (connection != null) (connection).Dispose();
                if (sqlitecommand != null) (sqlitecommand).Dispose();
            }
            return res;
        }
        //-----------------------------------------------------------------------------------------
        public List<string> StructTableDB
        {
            get { return _list; }
        }




    }
}
