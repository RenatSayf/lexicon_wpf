﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Lexicon.Model
{
    public class CollectionWords_Model : INotifyPropertyChanged
    {
        
        public CollectionWords_Model()
        {
            
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        
        private string _english;
        public string English
        {
            get { return _english; }
            set
            {
                if (value != _english)
                {
                    _english = value;
                    NotifyPropertyChanged("English");
                }
            }
        }
        
        private string _translation;
        public string Translation
        {
            get { return _translation; }
            set
            {
                if (value != _translation)
                {
                    _translation = value;
                    NotifyPropertyChanged("Translation");
                }
            }
        }
        
        private string _noun;
        public string Noun
        {
            get { return _noun; }
            set
            {
                if (value != _noun)
                {
                    _noun = value;
                    NotifyPropertyChanged("Noun");
                }
            }
        }
        
        private string _verb;
        public string Verb
        {
            get { return _verb; }
            set
            {
                if (value != _verb)
                {
                    _verb = value;
                    NotifyPropertyChanged("Verb");
                }
            }
        }
        
        private string _adjective;
        public string Adjective
        {
            get { return _adjective; }
            set
            {
                if (value != _adjective)
                {
                    _adjective = value;
                    NotifyPropertyChanged("Adjective");
                }
            }
        }
        
        private string _adverb;
        public string Adverb
        {
            get { return _adverb; }
            set
            {
                if (value != _adverb)
                {
                    _adverb = value;
                    NotifyPropertyChanged("Adverb");
                }
            }
        }
        
        private string _pronoun;
        public string Pronoun
        {
            get { return _pronoun; }
            set
            {
                if (value != _pronoun)
                {
                    _pronoun = value;
                    NotifyPropertyChanged("Pronoun");
                }
            }
        }
        
        private string _preposition;
        public string Preposition
        {
            get { return _preposition; }
            set
            {
                if (value != _preposition)
                {
                    _preposition = value;
                    NotifyPropertyChanged("Preposition");
                }
            }
        }
        
        public void Clean()
        {
            English = "";
            Translation = "";
            Noun = "";
            Verb = "";
            Adjective = "";
            Adverb = "";
            Preposition = "";
            Pronoun = "";
        }

        public void DoubleDelete()
        {
            if (_translation.Equals(_noun))
            {
                _translation = ""; return;
            }
            if (_translation.Equals(_verb))
            {
                _translation = ""; return;
            }
            if (_translation.Equals(_adjective))
            {
                _translation = ""; return;
            }
            if (_translation.Equals(_adverb))
            {
                _translation = ""; return;
            }
            if (_translation.Equals(_pronoun))
            {
                _translation = ""; return;
            }
            if (_translation.Equals(_pronoun))
            {
                _translation = ""; return;
            }
        }
       
    }
}
